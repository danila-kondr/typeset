workspace "odtgen"
	configurations { "Debug", "Release" }

	filter "configurations:Debug"
		symbols "On"
	
	filter "configurations:Release"
		defines { "NDEBUG" }

project "odtgen"
	kind "ConsoleApp"
	language "C"
	includedirs { "include/", "lib/include/" }

	files { "**.h", "**.c", "**.inc" }

	filter { "system:linux" }
		links { "m" }

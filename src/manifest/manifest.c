/**
 *  This file is a part of odtgen, program which converts MD to ODT.
 *  Copyright (C) 2022  Danila Kondratenko <dan.kondratenko2013@ya.ru>
 *
 *  odtgen is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  odtgen is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "manifest.h"

#include <utils/xalloc.h>
#include <utils/strdup.h>

#include <stdio.h>
#include <assert.h>

#define ODT_MIMETYPE "application/vnd.oasis.opendocument.text"

#define XMLNS(_namespace, _uri) " xmlns:" #_namespace "=\"" _uri "\""

#define OPENDOCUMENT_URN(_name, _version) \
	"urn:oasis:names:tc:opendocument:xmlns:" _name ":" _version

#define XML_SPEC "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"

#define MANIFEST_BEGIN "<manifest:manifest" \
	XMLNS(manifest, OPENDOCUMENT_URN("manifest", "1.0")) \
	" manifest:version=\"1.3\">\n"

#define MANIFEST_DOCUMENT_ENTRY " <manifest:file-entry " \
	"manifest:full-path=\"/\" manifest:version=\"1.3\" " \
	"manifest:media-type=\"" ODT_MIMETYPE "\" />\n"

#define MANIFEST_FILE_ENTRY_TEMPLATE \
	" <manifest:file-entry manifest:full-path=\"%s\" " \
	"manifest:media-type=\"%s\" />\n"

#define MANIFEST_END "</manifest:manifest>\n"

struct odf_manifest *odf_manifest_new()
{
	return xcalloc(1, sizeof(struct odf_manifest));
}

void odf_manifest_delete(struct odf_manifest **pmanifest)
{
	struct odf_manifest *manifest;
	struct odf_manifest_node *x, *p;

	assert(pmanifest);
	manifest = *pmanifest;

	if (!manifest)
		return;

	x = manifest->head;
	while (x) {
		p = x;
		x = x->next;

		xfree(p->name);
		xfree(p->mimetype);
		xfree(p);
	}
	xfree(manifest);
	*pmanifest = NULL;
}

void odf_manifest_append(struct odf_manifest *manifest,
	char *name, char *mimetype)
{
	assert(manifest);
	if (!manifest->head) {
		manifest->head = xcalloc(1, 
				sizeof(struct odf_manifest_node));
		manifest->tail = manifest->head;
	}
	else {
		manifest->tail->next = xcalloc(1,
				sizeof(struct odf_manifest_node));
		manifest->tail = manifest->tail->next;
	}

	manifest->tail->name = xstrdup(name);
	manifest->tail->mimetype = xstrdup(mimetype);
}

struct buffer *odf_manifest_xml(struct odf_manifest *manifest)
{
	struct buffer *result;
	struct odf_manifest_node *x;
	char file_entry[512];

	result = buffer_new();
	buffer_put_string(result, XML_SPEC);
	buffer_put_string(result, MANIFEST_BEGIN);

	buffer_put_string(result, MANIFEST_DOCUMENT_ENTRY);

	x = manifest->head;
	while (x) {
		sprintf(file_entry, MANIFEST_FILE_ENTRY_TEMPLATE,
			x->name, x->mimetype);
		buffer_put_string(result, file_entry);
		x = x->next;
	}

	buffer_put_string(result, MANIFEST_END);

	return result;
}

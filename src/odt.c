/**
 *  This file is a part of odtgen, program which converts MD to ODT.
 *  Copyright (C) 2022  Danila Kondratenko <dan.kondratenko2013@ya.ru>
 *
 *  odtgen is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  odtgen is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <renderer.h>

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <cJSON.h>
#include <cwalk.h>
#include <miniz.h>
#include <mustach/mustach-cjson.h>
#include <pikchr.h>

#include <manifest.h>
#include <rdf.h>
#include <odt/schema.h>
#include <odt/template.h>

#include <utils/error.h>
#include <utils/imginfo.h>
#include <utils/strdup.h>
#include <utils/xalloc.h>
#include <utils/mime.h>
#include <utils/tex.h>

static void odt_generate(struct renderer *renderer);
static void fodt_generate(struct renderer *renderer);

static int odt_enter_block(MD_BLOCKTYPE type, void *detail, void *data);
static int odt_leave_block(MD_BLOCKTYPE type, void *detail, void *data);
static int odt_enter_span(MD_SPANTYPE type, void *detail, void *data);
static int odt_leave_span(MD_SPANTYPE type, void *detail, void *data);
static int odt_text(MD_TEXTTYPE type, const MD_CHAR *text, MD_SIZE size, 
	void *data);
static void odt_write(struct renderer *renderer);
static void fodt_write(struct renderer *renderer);

static void begin_table(struct renderer *renderer, 
	MD_BLOCK_TABLE_DETAIL *detail);
static void put_outer_image(struct renderer *renderer, MD_SPAN_IMG_DETAIL *detail);
static void put_image(struct renderer *renderer, struct memfile *m,
	long width, long height);
static void put_formula(struct renderer *renderer);
static void escape_text(struct buffer *buf, const MD_CHAR *text, MD_SIZE size);
static void escape_code(struct buffer *buf, const MD_CHAR *text, MD_SIZE size);

static char *expand_path(char *parent, char *x);
static char *attr_text(MD_ATTRIBUTE *pAttr);

typedef int (*fence_cb)(struct renderer *renderer);

static int fence_pikchr(struct renderer *renderer);
static int fence_dot(struct renderer *renderer);

static struct {
	char fence_text[32];
	fence_cb cb;
} fences[] = {
	{ "pikchr", fence_pikchr },
	{ "dot", fence_dot },
	{ "", NULL },
};

static int process_fence(struct renderer *renderer)
{
	int i;

	for (i = 0; fences[i].cb; i++) {
		if (strcmp(renderer->fence_text, fences[i].fence_text) == 0)
			return fences[i].cb(renderer);
	}

	return 0;
}

void odt_init(struct renderer *renderer)
{
	renderer->parser.enter_block = odt_enter_block;
	renderer->parser.leave_block = odt_leave_block;
	renderer->parser.enter_span = odt_enter_span;
	renderer->parser.leave_span = odt_leave_span;
	renderer->parser.text = odt_text;

	switch (renderer->document_type) {
	case DOCUMENT_ODT:
		renderer->write = odt_write;
		break;
	case DOCUMENT_FLAT_ODT:
		renderer->write = fodt_write;
		break;
	default:;
	}
}

static cJSON *named_json(struct named x)
{
	char buffer[32];

	named_string(buffer, x);
	return cJSON_CreateString(buffer);
}

/* For template parameters cJSON will be used. */
static cJSON *renderer_parameters(struct renderer *renderer)
{
	char buffer[32];
	cJSON *object;
	cJSON *roman_font, *modern_font, *font_size, *modern_font_size;
	cJSON *paper_width, *paper_height;
	cJSON *margin_left, *margin_right, *margin_top, *margin_bottom;
	cJSON *list_style, *list_style_item, *indent, *margin, *style, *level;
	cJSON *text_indent, *footer_center_tab_stop, *footer_right_tab_stop;
	cJSON *line_height;
	int i;

	object = cJSON_CreateObject();

	roman_font = cJSON_CreateString("Liberation Serif");
	modern_font = cJSON_CreateString("Liberation Mono");
	font_size = named_json(renderer->font_size);
	modern_font_size = named_json(renderer->modern_font_size);
	paper_width = named_json(renderer->paper_size.width);
	paper_height = named_json(renderer->paper_size.height);
	margin_left = named_json(renderer->margins.left);
	margin_right = named_json(renderer->margins.right);
	margin_top = named_json(renderer->margins.top);
	margin_bottom = named_json(renderer->margins.bottom);
	text_indent = named_json(renderer->text_indent);
	footer_center_tab_stop = named_json(renderer->footer_center_tab_stop);
	footer_right_tab_stop = named_json(renderer->footer_right_tab_stop);

	sprintf(buffer, "%d%%", renderer->line_height);
	line_height = cJSON_CreateString(buffer);

	list_style = cJSON_CreateArray();
	for (i = 0; i < 10; i++) {
		level = cJSON_CreateNumber(i + 1);
		style = cJSON_CreateString(
			renderer->list_styles[i].numbering_name);
		indent = named_json(renderer->list_styles[i].text_indent);
		margin = named_json(renderer->list_styles[i].margin_left);

		list_style_item = cJSON_CreateObject();
		cJSON_AddItemToObject(list_style_item, "level", level);
		cJSON_AddItemToObject(list_style_item, "style", style); 
		cJSON_AddItemToObject(list_style_item, "indent", indent);
		cJSON_AddItemToObject(list_style_item, "margin", margin);

		cJSON_AddItemToArray(list_style, list_style_item);
	}

	cJSON_AddItemToObject(object, "RomanFont", roman_font);
	cJSON_AddItemToObject(object, "ModernFont", modern_font);
	cJSON_AddItemToObject(object, "RomanFontSize", font_size);
	cJSON_AddItemToObject(object, "ModernFontSize", modern_font_size);
	cJSON_AddItemToObject(object, "PaperWidth", paper_width);
	cJSON_AddItemToObject(object, "PaperHeight", paper_height);
	cJSON_AddItemToObject(object, "MarginLeft", margin_left);
	cJSON_AddItemToObject(object, "MarginRight", margin_right);
	cJSON_AddItemToObject(object, "MarginTop", margin_top);
	cJSON_AddItemToObject(object, "MarginBottom", margin_bottom);
	cJSON_AddItemToObject(object, "ListStyle", list_style);
	cJSON_AddItemToObject(object, "TextIndent", text_indent);
	cJSON_AddItemToObject(object, "LineHeight", line_height);
	cJSON_AddItemToObject(object, "TextAlign", cJSON_CreateString("justify"));
	
	if (renderer->flags & ODT_FLAG_FIRST_PAGE_NUMBER)
		cJSON_AddFalseToObject(object, "NoFirstPageNumber");
	else
		cJSON_AddTrueToObject(object, "NoFirstPageNumber");

	if (renderer->flags & ODT_FLAG_OUTLINE_NUMBER)
		cJSON_AddTrueToObject(object, "OutlineNumber");
	else
		cJSON_AddFalseToObject(object, "OutlineNumber");

	cJSON_AddFalseToObject(object, "NoFooter");
	cJSON_AddItemToObject(object, "FooterCenterTabStop", 
		footer_center_tab_stop);
	cJSON_AddItemToObject(object, "FooterRightTabStop", 
		footer_right_tab_stop);

	return object;
}

static void set_parameters(struct renderer *renderer)
{
	struct named x = renderer->text_indent;
	struct named y;
	int i;

	strcpy(renderer->list_styles[0].numbering_name, "1");
	for (i = 0; i < 10; i++) {
		y.value = x.value * i;
		y.unit = x.unit;

		if (i > 0)
			strcpy(renderer->list_styles[i].numbering_name, 
				"а, б, .., аа, аб, ... (ru)");

		renderer->list_styles[i].text_indent = x;
		renderer->list_styles[i].margin_left = y;
	}
}

static void odt_generate(struct renderer *renderer)
{
	cJSON *parameters;
	char *styles; size_t styles_size;
	char *content_header; size_t content_header_size;

	set_parameters(renderer);
	parameters = renderer_parameters(renderer);

	mustach_cJSON_mem(
		odt_document_styles_template, 0,
		parameters, 0,
		&styles, &styles_size);
	buffer_put(renderer->styles, styles, styles_size);
	xfree(styles);
	
	mustach_cJSON_mem(
		odt_document_content_header_template, 0,
		parameters, 0,
		&content_header, &content_header_size);
	buffer_put(renderer->header, content_header, content_header_size);
	xfree(content_header);

	buffer_put_string(renderer->footer, "</office:document-content>\n");

	cJSON_Delete(parameters);
}

static void fodt_generate(struct renderer *renderer)
{
	cJSON *parameters;
	char *header; size_t header_size;

	set_parameters(renderer);
	parameters = renderer_parameters(renderer);

	mustach_cJSON_mem(
		flat_odt_document_header_template, 0,
		parameters, 0,
		&header, &header_size);
	buffer_put(renderer->header, header, header_size);
	xfree(header);

	buffer_put_string(renderer->footer, "</office:document>\n");

	cJSON_Delete(parameters);
}

static void fodt_write(struct renderer *renderer)
{
	fodt_generate(renderer);

	fwrite(renderer->header->b, 1, renderer->header->pos, renderer->file);
	fwrite(renderer->content->b, 1, renderer->content->pos, renderer->file);
	fwrite(renderer->footer->b, 1, renderer->footer->pos, renderer->file);
}

static void odt_write(struct renderer *renderer)
{
	mz_zip_archive pack = {0};
	mz_bool ret;
	struct odf_manifest *manifest;
	struct buffer *t;
	struct memfile *m;
	const char *mimetype;
	int compression;
	size_t i;

	odt_generate(renderer);
	
	manifest = odf_manifest_new();

	ret = mz_zip_writer_init_cfile(&pack, renderer->file, 0);
	if (!ret)
		error(1, "miniz: %s",
			mz_zip_get_error_string(
				mz_zip_get_last_error(&pack)
			)
		);
	mz_zip_writer_add_mem(&pack, "mimetype",
		"application/vnd.oasis.opendocument.text", 39, 0);

	mz_zip_writer_add_mem(&pack, "manifest.rdf",
		manifest_rdf, strlen(manifest_rdf),
		MZ_DEFAULT_COMPRESSION);
	odf_manifest_append(manifest, "manifest.rdf", "application/rdf+xml");

	for (i = 0; i < hmlen(renderer->resources); i++) {
		m = renderer->resources[i].value;
		mimetype = mime_string(m->type);

		switch (m->type) {
		case IMAGE_PNG:
		case IMAGE_JPEG:
		case IMAGE_GIF:
			compression = 0;
			break;
		default:
			compression = 1;
		}

		mz_zip_writer_add_mem(&pack,
			renderer->resources[i].key,
			m->buf.b,
			m->buf.pos,
			compression
			? MZ_DEFAULT_COMPRESSION
			: MZ_NO_COMPRESSION);

		odf_manifest_append(manifest, renderer->resources[i].key,
			mimetype);
	}

	mz_zip_writer_add_mem(&pack, "styles.xml",
		renderer->styles->b,
		renderer->styles->pos,
		MZ_DEFAULT_COMPRESSION);
	odf_manifest_append(manifest, "styles.xml", "text/xml");

	t = buffer_new();
	buffer_put(t, renderer->header->b, renderer->header->pos);
	buffer_put(t, renderer->content->b, renderer->content->pos);
	buffer_put(t, renderer->footer->b, renderer->footer->pos);
	mz_zip_writer_add_mem(&pack, "content.xml", t->b, t->pos,
		MZ_DEFAULT_COMPRESSION);
	odf_manifest_append(manifest, "content.xml", "text/xml");
	buffer_delete(&t);

	t = odf_manifest_xml(manifest);
	mz_zip_writer_add_mem(&pack, "META-INF/manifest.xml", t->b, t->pos,
		MZ_DEFAULT_COMPRESSION);
	buffer_delete(&t);

	mz_zip_writer_finalize_archive(&pack);
	mz_zip_writer_end(&pack);


	odf_manifest_delete(&manifest);
}

static int odt_enter_block(MD_BLOCKTYPE type, void *detail, void *data)
{
	struct renderer *renderer = data;

	arrput(renderer->blocks, type);

	switch (type) {
	case MD_BLOCK_DOC:
		buffer_put_string(renderer->content, ODT_BODY_BEGIN);
		break;
	case MD_BLOCK_QUOTE:
		buffer_put_string(renderer->content, ODT_BLOCK_QUOTE_BEGIN);
		break;
	case MD_BLOCK_UL:
		buffer_put_string(renderer->content, ODT_UNORDERED_LIST_BEGIN);
		break;
	case MD_BLOCK_OL:
		buffer_put_string(renderer->content, ODT_ORDERED_LIST_BEGIN);
		break;
	case MD_BLOCK_LI:
		buffer_put_string(renderer->content, ODT_LIST_ITEM_BEGIN);
		break;
	case MD_BLOCK_HR:
		warning("render: unimplemented node MD_BLOCK_HR; skipping");
		/* ? */
		break;
	case MD_BLOCK_H:
		buffer_printf(renderer->content, ODT_HEADING_BEGIN_FORMAT,
			((MD_BLOCK_H_DETAIL *)detail)->level,
			((MD_BLOCK_H_DETAIL *)detail)->level);
		break;
	case MD_BLOCK_CODE:
		renderer->fence_type = 
			((MD_BLOCK_CODE_DETAIL *)detail)->fence_char;

		if (renderer->fence_type != '~') {
			buffer_put_string(renderer->content,
				ODT_CODE_BLOCK_BEGIN);
		} else {
			renderer->fence_text = attr_text(
				&((MD_BLOCK_CODE_DETAIL *)detail)->lang);
			renderer->fence = buffer_new();
		}
		break;
	case MD_BLOCK_HTML:
		error(-1, "render: HTML block has been parsed");
		break;
	case MD_BLOCK_P:
		buffer_put_string(renderer->content, ODT_PARAGRAPH_BEGIN);
		break;
	case MD_BLOCK_TABLE:
		begin_table(renderer, detail);
		break;
	case MD_BLOCK_THEAD:
		buffer_put_string(renderer->content, ODT_TABLE_HEADER_BEGIN);
		break;
	case MD_BLOCK_TBODY:
		buffer_put_string(renderer->content, ODT_TABLE_BODY_BEGIN);
		break;
	case MD_BLOCK_TR:
		buffer_put_string(renderer->content, ODT_TABLE_ROW_BEGIN);
		break;
	case MD_BLOCK_TH:
		buffer_put_string(renderer->content, ODT_TABLE_HEAD_CELL_BEGIN);
		break;
	case MD_BLOCK_TD:
		buffer_put_string(renderer->content, ODT_TABLE_CELL_BEGIN);
		break;
	}

	return 0;
}

static int odt_leave_block(MD_BLOCKTYPE type, void *detail, void *data)
{
	struct renderer *renderer = data;

	switch (type) {
	case MD_BLOCK_DOC:
		buffer_put_string(renderer->content, ODT_BODY_END);
		break;
	case MD_BLOCK_QUOTE:
		buffer_put_string(renderer->content, ODT_BLOCK_QUOTE_END);
		break;
	case MD_BLOCK_UL:
	case MD_BLOCK_OL:
		buffer_put_string(renderer->content, ODT_LIST_END);
		break;
	case MD_BLOCK_LI:
		buffer_put_string(renderer->content, ODT_LIST_ITEM_END);
		break;
	case MD_BLOCK_HR:
		break;
	case MD_BLOCK_CODE:
		if (renderer->fence_type != '~') {
			buffer_put_string(renderer->content, ODT_PARAGRAPH_END);
		} else {
			if (!process_fence(renderer)) {
				warning("render: incorrect fence: %s",
					renderer->fence_text);
				buffer_put_string(renderer->content,
					ODT_CODE_BLOCK_BEGIN);
				escape_code(renderer->content,
					renderer->fence->b,
					renderer->fence->pos);
				buffer_put_string(renderer->content,
					ODT_CODE_BLOCK_END);
			}

			xfree(renderer->fence_text);
			buffer_delete(&renderer->fence);
		}
		renderer->fence_type = 0;
		break;
	case MD_BLOCK_P:
		buffer_put_string(renderer->content, ODT_PARAGRAPH_END);
		break;
	case MD_BLOCK_H:
		buffer_put_string(renderer->content, ODT_HEADING_END);
		break;
	case MD_BLOCK_HTML: /* Won't parse */
		break;
	case MD_BLOCK_TABLE:
		buffer_put_string(renderer->content, ODT_TABLE_END);
		break;
	case MD_BLOCK_THEAD:
		buffer_put_string(renderer->content, ODT_TABLE_HEADER_END);
		break;
	case MD_BLOCK_TBODY:
		buffer_put_string(renderer->content, ODT_TABLE_BODY_END);
		break;
	case MD_BLOCK_TR:
		buffer_put_string(renderer->content, ODT_TABLE_ROW_END);
		break;
	case MD_BLOCK_TH:
	case MD_BLOCK_TD:
		buffer_put_string(renderer->content, ODT_TABLE_CELL_END);
		break;
	}

	arrpop(renderer->blocks);

	return 0;
}

static int odt_enter_span(MD_SPANTYPE type, void *detail, void *data)
{
	struct renderer *renderer = data;
	size_t len;

	arrput(renderer->spans, type);

	switch (type) {
	case MD_SPAN_EM:
	case MD_SPAN_STRONG:
		len = arrlenu(renderer->spans);
		/* If <strong> is inside <em>, don't write */
		/* Span style will be placed in text(...) */
		if (len > 1 && renderer->spans[len-2] == MD_SPAN_EM)
			break;
		buffer_put_string(renderer->content, ODT_SPAN_FIRST_HALF);
		break;
	case MD_SPAN_A:
		buffer_put_string(renderer->content, ODT_LINK_FIRST_HALF);
		buffer_put(renderer->content,
				((MD_SPAN_A_DETAIL *)detail)->href.text,
				((MD_SPAN_A_DETAIL *)detail)->href.size);
		buffer_put_string(renderer->content, ODT_LINK_BEGIN_TAIL);
		break;
	case MD_SPAN_IMG:
		put_outer_image(renderer, detail);
		break;
	case MD_SPAN_CODE:
		buffer_put_string(renderer->content, ODT_CODE_SPAN_BEGIN);
		break;
	case MD_SPAN_LATEXMATH:
		renderer->formula_type = FORMULA_TEXT;
		break;
	case MD_SPAN_LATEXMATH_DISPLAY:
		renderer->formula_type = FORMULA_DISPLAY;
		break;
	default:
		warning("render: unrecognized span type #%d", type);
	}

	return 0;
}

static int odt_leave_span(MD_SPANTYPE type, void *detail, void *data)
{
	struct renderer *renderer = data;
	int top;
	size_t len;

	top = arrpop(renderer->spans);
	len = arrlenu(renderer->spans);

	switch (type) {
	case MD_SPAN_EM:
		buffer_put_string(renderer->content, ODT_SPAN_END);
		break;
	case MD_SPAN_STRONG:
		if (len > 0 && renderer->spans[len-1] != MD_SPAN_EM) {
			buffer_put_string(renderer->content, ODT_SPAN_END);
		}
		break;
	case MD_SPAN_CODE:
		buffer_put_string(renderer->content, ODT_SPAN_END);
		break;
	case MD_SPAN_A:
		buffer_put_string(renderer->content, ODT_LINK_END);
		break;
	case MD_SPAN_IMG:
		break;
	case MD_SPAN_LATEXMATH:
	case MD_SPAN_LATEXMATH_DISPLAY:
		put_formula(renderer);
		renderer->formula_type = FORMULA_NOTHING;
		buffer_clear(renderer->formula);
		break;
	default:;
	}

	return 0;
}

static int odt_text(MD_TEXTTYPE type, const MD_CHAR *text, MD_SIZE size,
	void *data)
{
	struct renderer *renderer = data;
	int top1 = -1, top2 = -1;

	if (arrlen(renderer->spans))
		top1 = arrpop(renderer->spans);
	if (top1 == MD_SPAN_STRONG) {
		if (arrlen(renderer->spans) > 0) {
			top2 = arrpop(renderer->spans);
			if (top2 == MD_SPAN_EM) {			
				buffer_put_string(renderer->content,
					ODT_SPAN_DOUBLE_BEGIN);
			} else {
				buffer_put_string(renderer->content,
					ODT_SPAN_STRONG_BEGIN);
			}
			arrput(renderer->spans, top2);
		} else {
			buffer_put_string(renderer->content,
				ODT_SPAN_STRONG_BEGIN);
		}
	} else if (top1 == MD_SPAN_EM) {
		buffer_put_string(renderer->content, ODT_SPAN_EM_BEGIN);
	} else if (top1 == MD_SPAN_IMG) {
		arrput(renderer->spans, top1);
		return 0;
	}

	arrput(renderer->spans, top1);

	switch (type) {
	case MD_TEXT_SOFTBR:
		buffer_put_string(renderer->content, " ");
		break;
	case MD_TEXT_BR:
		buffer_put_string(renderer->content, ODT_LINE_BREAK "\n");
		break;
	case MD_TEXT_NULLCHAR:
		buffer_put_string(renderer->content, "\xef\xbf\xbd");
		break;
	case MD_TEXT_NORMAL:
		escape_text(renderer->content, text, size);
		break;
	case MD_TEXT_ENTITY:
		buffer_put(renderer->content, text, size);
		break;
	case MD_TEXT_CODE:
		if (renderer->fence_type != '~')
			escape_code(renderer->content, text, size);
		else
			buffer_put(renderer->fence, text, size);
		break;
	case MD_TEXT_LATEXMATH:
		buffer_put(renderer->formula, text, size);
		break;
	default:
		warning("render: unrecognized text type #%d", type);
	}

	return 0;
}

static void begin_table(struct renderer *renderer, 
	MD_BLOCK_TABLE_DETAIL *detail)
{
	int i;

	buffer_put_string(renderer->content, ODT_TABLE_BEGIN);

	for (i = 0; i < detail->col_count; i++) {
		buffer_put_string(renderer->content, ODT_TABLE_COLUMN);
	}
}

static void put_outer_image(struct renderer *renderer, MD_SPAN_IMG_DETAIL *detail)
{
	char *img_name, *img_path;
	struct memfile *m;

	assert(renderer->document_type == DOCUMENT_ODT
		|| renderer->document_type == DOCUMENT_FLAT_ODT);

	img_name = xstrndup(detail->src.text, detail->src.size);
	img_path = expand_path(renderer->input_dirname, img_name);

	if (renderer->flags & PROGRAM_FLAG_VERBOSE)
		fprintf(stderr, "Reading file %s\n", img_path);

	m = memfile_read(img_path);
	put_image(renderer, m, 0, 0);

	xfree(img_name);
	xfree(img_path);
}

static void put_image(struct renderer *renderer, struct memfile *m, 
	long width, long height)
{
	char img_inner_name[32] = {0};
	struct img_info info;

	assert(renderer->document_type == DOCUMENT_ODT
		|| renderer->document_type == DOCUMENT_FLAT_ODT);

	if (!width && !height) {
		if (!img_info(&info, m))
			error(1, "render: invalid image format");
		else
			m->type = info.mime_type;
	} else {
		info.width = width;
		info.height = height;
	}

	buffer_printf(renderer->content,
		ODT_FRAME_GRAPHICS_BEGIN_FORMAT,
		info.width, info.height);

	switch (renderer->document_type) {
	case DOCUMENT_ODT:	
		sprintf(img_inner_name, "Pictures/%d", renderer->n_images);

		buffer_printf(renderer->content, ODT_IMAGE_BEGIN_FORMAT, 
			img_inner_name);
		shput(renderer->resources, img_inner_name, m);

		renderer->n_images++;
		break;
	case DOCUMENT_FLAT_ODT:
		buffer_put_string(renderer->content, FLAT_ODT_IMAGE_BEGIN);
		buffer_put_string(renderer->content, ODT_BINARY_DATA_BEGIN);
		buffer_put_base64(renderer->content, m->buf.b, m->buf.pos);
		buffer_put_string(renderer->content, ODT_BINARY_DATA_END);
		buffer_put_string(renderer->content, FLAT_ODT_IMAGE_END);
		memfile_delete(&m);
	default:;
	}

	buffer_put_string(renderer->content, ODT_FRAME_END);

}

static void put_formula(struct renderer *renderer)
{
	struct memfile *formula;
	char img_inner_name[32] = {0};
	struct img_info info;
	int mode = 0;

	assert(renderer->document_type == DOCUMENT_ODT
		|| renderer->document_type == DOCUMENT_FLAT_ODT);

	if (renderer->formula_type == FORMULA_NOTHING)
		return;
	switch (renderer->formula_type) {
	case FORMULA_TEXT:
		mode = 0;
		break;
	case FORMULA_DISPLAY:
		mode = 1;
		break;
	}

	formula = tex_render_formula(
		renderer->formula->b,
		renderer->formula->pos,
		0,
		mode);
	img_info(&info, formula);

	buffer_printf(renderer->content,
		ODT_FRAME_FORMULA_BEGIN_FORMAT,
		info.width, info.height);

	switch (renderer->document_type) {
	case DOCUMENT_ODT:	
		sprintf(img_inner_name, "Pictures/%d", renderer->n_images);

		buffer_printf(renderer->content, ODT_IMAGE_BEGIN_FORMAT,
			img_inner_name);
		shput(renderer->resources, img_inner_name, formula);

		renderer->n_images++;
		break;
	case DOCUMENT_FLAT_ODT:
		buffer_put_string(renderer->content, FLAT_ODT_IMAGE_BEGIN);
		buffer_put_string(renderer->content, ODT_BINARY_DATA_BEGIN);
		buffer_put_base64(renderer->content, 
				formula->buf.b, formula->buf.pos);
		buffer_put_string(renderer->content, ODT_BINARY_DATA_END);
		buffer_put_string(renderer->content, FLAT_ODT_IMAGE_END);
		memfile_delete(&formula);
	default:;
	}

	buffer_put_string(renderer->content, ODT_FRAME_END);
}

static void escape_text(struct buffer *buf, const MD_CHAR *text, MD_SIZE size)
{
	char *str = text;
	size_t len = size;

	while (len) {
		switch (*str) {
		case '<':
			buffer_put_string(buf, "&lt;");
			break;
		case '>':
			buffer_put_string(buf, "&gt;");
			break;
		case '\'':
			buffer_put_string(buf, "&apos;");
			break;
		case '"':
			buffer_put_string(buf, "&quot;");
			break;
		case '&':
			buffer_put_string(buf, "&amp;");
			break;
		default:
			buffer_put_byte(buf, *str);
		}

		str++; len--;
	}
}

static void escape_code(struct buffer *buf, const MD_CHAR *text, MD_SIZE size)
{
	int n = 0, l = 0;
	char *str;
	size_t len;

	str = text;
	len = size;

	while (len) {
		if (*str != ' ')
			break;
		n++;
		str++;
		len--;
	}

	str -= (n & 3);
	size += (n % 3);
	n &= ~3;
	for (; n; n -= 4)
		buffer_put_string(buf, ODT_TAB);

	while (len) {
		switch (*str) {
		case '\n':
			buffer_put_string(buf, ODT_LINE_BREAK);
			break;
		case '\t':
			buffer_put_string(buf, ODT_TAB);
			break;
		case '<':
			buffer_put_string(buf, "&lt;");
			break;
		case '>':
			buffer_put_string(buf, "&gt;");
			break;
		case '&':
			buffer_put_string(buf, "&amp;");
			break;
		case '\'':
			buffer_put_string(buf, "&apos;");
			break;
		case '"':
			buffer_put_string(buf, "&quot;");
			break;
		default:
			buffer_put_byte(buf, *str);
		}

		str++;
		len--;
	}
}

static char *expand_path(char *parent, char *x)
{
	char *result = NULL;
	size_t result_len;

	if (cwk_path_is_absolute(x))
		return xstrdup(x);

	result_len = strlen(x) + strlen(parent) + 4;
	result = xcalloc(result_len, sizeof(char));
	cwk_path_join(parent, x, result, result_len);

	return result;
}

static int fence_pikchr(struct renderer *renderer)
{
	struct memfile *m;
	char *svg;
	int svg_w, svg_h;

	if (renderer->flags & PROGRAM_FLAG_VERBOSE)
		fprintf(stderr, "Running pikchr\n");

	svg = pikchr(renderer->fence->b, 
		NULL, 
		PIKCHR_PLAINTEXT_ERRORS, 
		&svg_w, 
		&svg_h);

	if (!svg)
		error(1, "pikchr: too few memory");

	if (svg_w < 0)
		error(1, "pikchr:\n%s", svg);

	m = memfile_new();
	memfile_put(m, svg, strlen(svg));
	m->type = IMAGE_SVG_XML;
	xfree(svg);

	buffer_put_string(renderer->content, ODT_FIGURE_BEGIN);
	put_image(renderer, m, svg_w, svg_h);
	buffer_put_string(renderer->content, ODT_FIGURE_END);
	return 1;
}

static fence_dot(struct renderer *renderer)
{
	char dot_out[16] = "dotXXXXXX.svg";
	char dot_cmd[256];
	int i, r;
	FILE *dot;
	struct memfile *m;
	
	srand(time(NULL));	
	for (i = 0; dot_out[i]; i++) {
		if (dot_out[i] == 'X')
			dot_out[i] = ((r=rand()%52) >= 26)
				? ('a'+r-26)
				: ('A'+r);
	}

	sprintf(dot_cmd, "dot -Tsvg -o%s", dot_out);
	dot = popen(dot_cmd, "w");
	if (!dot)
		error(1, "dot: %s", strerror(errno));
	fwrite(renderer->fence->b, 1, renderer->fence->pos, dot);
	fclose(dot);

	m = memfile_read(dot_out);
	remove(dot_out);

	buffer_put_string(renderer->content, ODT_FIGURE_BEGIN);
	put_image(renderer, m, 0, 0);
	buffer_put_string(renderer->content, ODT_FIGURE_END);
	return 1;
}

static char *attr_text(MD_ATTRIBUTE *pAttr)
{
	return xstrndup(pAttr->text, pAttr->size);
}

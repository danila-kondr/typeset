/**
 *  This file is a part of odtgen, program which converts MD to ODT.
 *  Copyright (C) 2022  Danila Kondratenko <dan.kondratenko2013@ya.ru>
 *
 *  odtgen is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  odtgen is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#define  _CRT_SECURE_NO_WARNINGS
#include "version.h"

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <errno.h>

#include <argparse.h>
#include <cwalk.h>

#include <utils/xalloc.h>
#include <utils/strdup.h>
#include <utils/error.h>
#include <utils/named.h>

#include <renderer.h>

void parse_margins(struct renderer *, char *);

char *outfilename(char *, int);

#define ODT_PACKED 0
#define ODT_FLAT 1

void version(struct argparse *p, struct argparse_option *opt)
{
	printf("odtgen v" _ODTGEN_VER_STR "\n");
	printf("Copyright (c) 2022-2023 Danila Kondratenko\n");
	exit(0);
}

static const char *const usages[] = {
	"odtgen [options] input_file",
	NULL
};

int main(int argc, char **argv)
{
	char *input_name = NULL, *output_name = NULL, *out_name_buf = NULL;
	char *margins = NULL;

	struct renderer odt;
	int i, opt, type = ODT_PACKED, font_size = 14;
	unsigned odt_flags = 0;
	int n_args;

	struct argparse argparse;
	struct argparse_option options[] = {
		OPT_HELP(),
		OPT_BIT('F', "formula", &odt_flags,
				"render formulas with LaTeX",
				NULL, PROGRAM_FLAG_FORMULAS, 0),
		OPT_BOOLEAN('f', NULL, &type, "generate flat ODT document",
				NULL, 0, 0),
		OPT_STRING('M', "margin", &margins,
				"set page margins",
				NULL, 0, 0),
		OPT_BIT('n', NULL, &odt_flags,
				"insert outline numbers",
				NULL, ODT_FLAG_OUTLINE_NUMBER, 0),
		OPT_STRING('o', NULL, &out_name_buf,
				"specify output file name",
				NULL, 0, 0),
		OPT_BIT('p', NULL, &odt_flags, 
				"insert page number on the first page",
				NULL, ODT_FLAG_FIRST_PAGE_NUMBER, 0),
		OPT_INTEGER('s', "font-size", &font_size,
				"set font size (in points)",
				NULL, 0, 0),
		OPT_BIT('v', "verbose", &odt_flags,
				"print more information",
				NULL, PROGRAM_FLAG_VERBOSE, 0),
		OPT_BOOLEAN(0, "version", NULL,
				"show version", version, 0, 0),
		OPT_END()
	};

	error_set_progname(argv[0]);
	argparse_init(&argparse, options, usages, 0);
	argparse_describe(&argparse, 
			"This program converts text documents "
			"from Markdown to ODT.", 
			NULL);

	argc = argparse_parse(&argparse, argc, argv);

	input_name = *argv;
	if (!input_name)
		error(1, "input file not provided");

	if (out_name_buf)
		output_name = xstrdup(out_name_buf);
	else
		output_name = outfilename(input_name, type);

	odt.flags = odt_flags;
	switch (type) {
	case ODT_PACKED:
		renderer_init(&odt, DOCUMENT_ODT);
		break;
	case ODT_FLAT:
		renderer_init(&odt, DOCUMENT_FLAT_ODT);
		break;
	}

	odt.paper_size.width = named(210, UNIT_MM);
	odt.paper_size.height = named(297, UNIT_MM);
	odt.line_height = 150;
	odt.text_indent = named(12.5, UNIT_MM);


	/* Naverno, 80 simvolov vlezet. */
	odt.modern_font_size = named(10, UNIT_PT); 
	odt.font_size = named(font_size, UNIT_PT);

	if (margins) {
		parse_margins(&odt, margins);
	} else {
		odt.margins.top = named(20, UNIT_MM);
		odt.margins.bottom = named(20, UNIT_MM);
		odt.margins.left = named(30, UNIT_MM);
		odt.margins.right = named(15, UNIT_MM);
	}
	odt.text_size.width = named_sub(
			odt.paper_size.width,
			named_add(
				odt.margins.left,
				odt.margins.right
			));
	odt.text_size.height = named_sub(
			odt.paper_size.height,
			named_add(
				odt.margins.top,
				odt.margins.bottom
			));

	odt.footer_center_tab_stop = odt.text_size.width;
	odt.footer_center_tab_stop.value >>= 1;

	odt.footer_right_tab_stop = odt.text_size.width;

	render(&odt, input_name, output_name);	

	if (odt_flags & PROGRAM_FLAG_VERBOSE)
		fprintf(stderr, "Result is at %s\n", output_name);

	free(output_name);
	return 0;
}

char *outfilename(char *input, int type)
{
	size_t len, result_len;
	char *base, *result, *new_ext;

	if (strcmp(input, "-") == 0)
		error(1, "please provide output file name");

	cwk_path_get_basename(input, &base, &len);

	result_len = len + 5;
	result = xcalloc(result_len, sizeof(char));
	
	switch (type) {
	case ODT_PACKED: new_ext = "odt"; break;
	case ODT_FLAT: new_ext = "fodt"; break;
	}

	cwk_path_change_extension(base, new_ext, result, result_len);
	return result;
}

void parse_margins(struct renderer *doc, char *x)
{
	char *tokens[4] = {0};
	struct named vals[4];
	int i = 0;

	tokens[0] = strtok(x, ",");
	for (i = 0; (i < 4) && tokens[i]; i++) {
		if (!named_scan(&vals[i], tokens[i]))
			error(1, "invalid margin specifier");
		tokens[i] = strtok(NULL, ",");
	}

	switch (i) {
	case 1:
		doc->margins.left =
			doc->margins.right =
			doc->margins.top = 
			doc->margins.bottom =
			vals[0];
		break;
	case 2:
		doc->margins.top =
			doc->margins.bottom = vals[0];
		doc->margins.left =
			doc->margins.right = vals[1];
		break;
	case 3:
		doc->margins.top = vals[0];
		doc->margins.right =
			doc->margins.left = vals[1];
		doc->margins.bottom = vals[2];
		break;
	case 4:
		doc->margins.top = vals[0];
		doc->margins.right = vals[1];
		doc->margins.bottom = vals[2];
		doc->margins.left = vals[3];
		break;
	}	

}

/**
 *  This file is a part of odtgen, program which converts MD to ODT.
 *  Copyright (C) 2022  Danila Kondratenko <dan.kondratenko2013@ya.ru>
 *
 *  odtgen is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  odtgen is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <utils/readtext.h>

#include <utils/error.h>

#include <inttypes.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>

static FILE *open_file(char *filename);

#define UTF_16LE 1
#define UTF_16BE 2
static int isu16(FILE *f)
{
	int result = 0;
	uint8_t buf[2];

	fread(buf, 1, 2, f);
	if (buf[0] == 0xFF && buf[1] == 0xFE) {
		result = UTF_16LE;
	} else if (buf[0] == 0xFE && buf[1] == 0xFF) {
		result = UTF_16BE;
	} else {
		ungetc(buf[1], f);
		ungetc(buf[0], f);
	}

	return result;
}

static void read_u16le(struct buffer *buf, FILE *f);
static void read_u16be(struct buffer *buf, FILE *f);
static void read_u8(struct buffer *buf, FILE *f);

struct buffer *read_text(char *filename)
{
	struct buffer *buf;
	FILE *f;
	int type;

	buf = buffer_new();
	f = open_file(filename);
	type = isu16(f);
	if (type == UTF_16LE)
		read_u16le(buf, f);
	else if (type == UTF_16BE)
		read_u16be(buf, f);
	else
		read_u8(buf, f);

	if (f != stdin)
		fclose(f);
	return buf;
}

#define IS_H_SURROGATE(x) ( (x) >= 0xD800 && (x) <= 0xDBFF )
#define IS_L_SURROGATE(x) ( (x) >= 0xDC00 && (x) <= 0xDFFF )
#define IS_SYMBOL(x) ( !((x)>=0xD800 && (x)<=0xDFFF) )

typedef long(*get_codepoint_fn)(uint8_t *, size_t, size_t);

static void read_u16(struct buffer *buf, FILE *f, get_codepoint_fn getcp)
{
	uint8_t chunk[4098] = {0};
	size_t n_read = 0;
	size_t l_surrogate = 0;
	size_t i = 0;
	int32_t uchar = 0;

	long codepoint = 0;
	while (!feof(f)) {
		n_read = fread(chunk, 1, 4096, f);
		if (n_read == 0)
			break;

		if (n_read == 4096) {
			codepoint = getcp(chunk, n_read-2, n_read);

			if (IS_H_SURROGATE(codepoint)) {
				l_surrogate =
					fread(&chunk[n_read], 1, 2, f);
			}
		}

		for (i = 0; i < n_read+l_surrogate; i += 2) {
			codepoint = getcp(chunk, i, n_read+l_surrogate);
			if (IS_H_SURROGATE(codepoint)) {
				uchar = (codepoint & 0x3ff) << 10;
				i += 2;
				if (i >= n_read+l_surrogate) {
					buffer_put_u8char(buf, 0xFFFD);
					break;
				}

				codepoint = getcp(chunk, i, 
						n_read+l_surrogate);
				if (IS_L_SURROGATE(codepoint)) {
					uchar |= codepoint & 0x3ff;
					uchar += 0x10000;
				}
				else {
					buffer_put_u8char(buf, 0xFFFD);
					uchar = codepoint;
				}
			}
			else {
				uchar = codepoint;
			}

			buffer_put_u8char(buf, uchar);
		}
	}
}

static long getcp_u16le(uint8_t *b, size_t x, size_t size)
{
	long result = 0;
	size_t i = x;

	if (i >= size) 
		return -1L;
	result = b[i++];

	if (i >= size)
		return -1L;
	result |= (b[i++] << 8);

	return result;
}

static long getcp_u16be(uint8_t *b, size_t x, size_t size)
{
	long result = 0;
	size_t i = x;

	if (i >= size) 
		return -1L;
	result = (b[i++] << 8);

	if (i >= size)
		return -1L;
	result |= b[i++];

	return result;
}

static void read_u16le(struct buffer *buf, FILE *f)
{
	read_u16(buf, f, getcp_u16le);
}

static void read_u16be(struct buffer *buf, FILE *f)
{
	read_u16(buf, f, getcp_u16be);
}

static void read_u8(struct buffer *buf, FILE *f)
{
	uint8_t chunk[4096];
	size_t n_read = 0;
	size_t i;

	while (!feof(f)) {
		n_read = fread(chunk, 1, 4096, f);
		if (n_read == 0)
			break;
		buffer_put(buf, chunk, n_read);
	}
}

static FILE *open_file(char *name)
{
	FILE *f;

	if (strcmp(name, "-") == 0)
		return stdin;
	
	f = fopen(name, "rb");
	if (!f)
		error(1, strerror(errno));

	return f;
}


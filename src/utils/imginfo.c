/**
 *  This file is a part of odtgen, program which converts MD to ODT.
 *  Copyright (C) 2022  Danila Kondratenko <dan.kondratenko2013@ya.ru>
 *
 *  odtgen is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  odtgen is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <utils/imginfo.h>
#include <utils/xalloc.h>
#include <utils/error.h>

#include <nanosvg.h>

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>
#include <errno.h>

static int img_info_png(struct img_info *pdest, struct memfile *m);
static int img_info_gif(struct img_info *pdest, struct memfile *m);
static int img_info_jpeg(struct img_info *pdest, struct memfile *m);
static int img_info_bmp(struct img_info *pdest, struct memfile *m);

static int img_info_svg(struct img_info *pdest, struct memfile *m);
static int img_info_eps(struct img_info *pdest, struct memfile *m);

static int get_format(struct memfile *m);

int img_info(struct img_info *pdest, struct memfile *m)
{
	int ret = 1;

	assert(pdest);
	/* Determine format */
	pdest->mime_type = get_format(m);
	switch (pdest->mime_type) {
	case IMAGE_PNG:
		ret = img_info_png(pdest, m);
		break;
	case IMAGE_GIF:
		ret = img_info_gif(pdest, m);
		break;
	case IMAGE_JPEG:
		ret = img_info_jpeg(pdest, m);
		break;
	case IMAGE_SVG_XML:
		ret = img_info_svg(pdest, m);
		break;
	case IMAGE_BMP:
		ret = img_info_bmp(pdest, m);
		break;
	case IMAGE_X_EPS:
		ret = img_info_eps(pdest, m);
		break;
	default:
		return 0;
	}

	return ret;
}

#define COMPARE_6(_p, _a, _b, _c, _d, _e, _f) ( \
		((_p)[0] == (_a))               \
	&&	((_p)[1] == (_b))               \
	&&	((_p)[2] == (_c))               \
	&&	((_p)[3] == (_d))               \
	&&	((_p)[4] == (_e))               \
	&&	((_p)[5] == (_f)) )

#define COMPARE_8(_p, _a, _b, _c, _d, _e, _f, _g, _h) ( \
		((_p)[0] == (_a))                       \
	&&	((_p)[1] == (_b))                       \
	&&	((_p)[2] == (_c))                       \
	&&	((_p)[3] == (_d))                       \
	&&	((_p)[4] == (_e))                       \
	&&	((_p)[5] == (_f))                       \
	&&	((_p)[6] == (_g))                       \
	&&	((_p)[7] == (_h)) )

static int get_format(struct memfile *m)
{
	uint8_t *x, *p;
	size_t size;

	size = BUFFER(m)->pos;
	x = BUFFER(m)->b;
	p = x;

	if (COMPARE_6(p, 'G', 'I', 'F', '8', '7', 'a'))
		return IMAGE_GIF;

	if (COMPARE_6(p, 'G', 'I', 'F', '8', '9', 'a'))
		return IMAGE_GIF;

	if (p[0] == 0xFF && p[1] == 0xD8)
		return IMAGE_JPEG;

	if (COMPARE_6(p, '<', '?', 'x', 'm', 'l', ' '))
		return IMAGE_SVG_XML;

	if (strncmp(p, "\x89PNG\r\n\x1a\n", 8) == 0)
		return IMAGE_PNG;

	if (strncmp(p, "%!PS-Adobe-2.0 EPSF-1.2", 23) == 0)
		return IMAGE_X_EPS;

	if (strncmp(p, "%!PS-Adobe-2.0 EPSF-2.0", 23) == 0)
		return IMAGE_X_EPS;

	if (strncmp(p, "%!PS-Adobe-3.0 EPSF-3.0", 23) == 0)
		return IMAGE_X_EPS;

	if (strncmp(p, "%!PS-Adobe-3.0 EPSF-3.1", 23) == 0)
		return IMAGE_X_EPS;

	return APPLICATION_OCTET_STREAM;
}

#define FOURCC(a, b, c, d) ( (long)((a) | ((b)<<8) | ((c)<<16) | ((d)<<24)) )

#define NETWORK_INT32(x) (\
	((x)[3]) | ((x)[2] << 8) | ((x)[1] << 16) | ((x)[0] << 24) )
#define IHDR FOURCC('R', 'D', 'H', 'I')

static int img_info_png(struct img_info *pdest, struct memfile *m)
{
	uint8_t *p;
	uint32_t length, type, width, height;

	p = BUFFER(m)->b + 8;

	length = NETWORK_INT32(p);
	type   = NETWORK_INT32(p+4);

	if (type != IHDR)
		return 0;
	if (length != 13)
		return 0;

	width  = NETWORK_INT32(p+8);
	height = NETWORK_INT32(p+12);

	pdest->width = width;
	pdest->height = height;

	fprintf(stderr, "imginfo: image %dx%d\n", width, height);

	if (width == 0)
		return 0;
	if (height == 0)
		return 0;

	return 1;
}

#define LE_INT16(x) ( ((x)[0]) | ((x)[1] << 8) )
#define BE_INT16(x) ( ((x)[1]) | ((x)[0] << 8) )

static int img_info_gif(struct img_info *pdest, struct memfile *m)
{
	uint8_t *p;
	uint16_t width, height;

	p = BUFFER(m)->b + 6;
	
	width = LE_INT16(p);
	height = LE_INT16(p+2);

	pdest->width = width;
	pdest->height = height;

	return 1;
}

static int img_info_jpeg(struct img_info *pdest, struct memfile *m)
{
	uint8_t *p, *end;
	uint16_t width, height, marker = 0xFFD8, length;

	p = BUFFER(m)->b + 2;
	end = BUFFER(m)->b + BUFFER(m)->pos;
	while (p < end) {
		marker = BE_INT16(p);
		if (marker == 0xFFC0 || marker == 0xFFC1)
			break;

		length = BE_INT16(p+2);

		p += length + 2;
	}

	if (p >= end)
		return 0;

	p += 4;
	height = BE_INT16(p+1);
	width = BE_INT16(p+1);	

	return 1;
}

#define LE_INT32(x) (\
	((x)[0]) | ((x)[1] << 8) | ((x)[2] << 16) | ((x)[3] << 24) )

static int img_info_bmp(struct img_info *pdest, struct memfile *m)
{
	uint8_t *p;
	int32_t width, height;

	p = BUFFER(m)->b + 14;
	width = LE_INT32(p + 2);
	height = LE_INT32(p + 6);

	pdest->width = width;
	pdest->height = height;

	return 1;
}

static int img_info_svg(struct img_info *pdest, struct memfile *m)
{
	char *svg_file;
	NSVGimage *svg;

	svg_file = xcalloc(BUFFER(m)->pos + 1, sizeof(char));
	memcpy(svg_file, BUFFER(m)->b, BUFFER(m)->pos);

	svg = nsvgParse(svg_file, "px", 96);
	if (!svg)	
		error(1, "nanosvg: %s", strerror(errno));
	pdest->width = (long)(svg->width + 0.5);
	pdest->height = (long)(svg->height + 0.5);
	nsvgDelete(svg);	

	xfree(svg_file);
	return 1;
}

static int img_info_eps(struct img_info *pdest, struct memfile *m)
{
	int llx, lly, urx, ury;
	char *p, *end, *tok;
	int ret = 0;

	p = xcalloc(BUFFER(m)->pos+1, sizeof(uint8_t));
	memcpy(p, BUFFER(m)->b, BUFFER(m)->pos);

	tok = strtok(p, "\n");
	while (tok) {
		ret = sscanf(tok, 
			"%%%%BoundingBox: %d %d %d %d", &llx, &lly, &urx, &ury);
		if (ret == 4)
			break;
		
		tok = strtok(NULL, "\n");
	}

	xfree(p);

	pdest->width = (urx - llx) * 4 / 3;
	pdest->height = (ury - lly) * 4 / 3;

	return ret ? 1 : 0;
}

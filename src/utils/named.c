/**
 *  This file is a part of odtgen, program which converts MD to ODT.
 *  Copyright (C) 2022  Danila Kondratenko <dan.kondratenko2013@ya.ru>
 *
 *  odtgen is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  odtgen is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <utils/named.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct named named(double value, int unit)
{
	struct named result;

	result.unit = unit;
	switch (unit) {
	case UNIT_MM:
		result.value = value * NM_MILLIMETER;
		break;
	case UNIT_CM:
		result.value = value * NM_CENTIMETER;
		break;
	case UNIT_IN:
		result.value = value * NM_INCH;
		break;
	case UNIT_PT:
		result.value = value * NM_POINT;
		break;
	case UNIT_PC:
		result.value = value * NM_PICA;
		break;
	case UNIT_PX:
		result.value = value * NM_PIXEL;
		break;
	}

	return result;
}

static struct {
	long mul, div;
} convert[6][6] = {

{ {1,1},    {1,10},    {10,254}, {720,254}, {120,254}, {960,254} },
{ {10,1},   {1,1},     {100,254},{7200,254},{1200,254},{9600,254} },
{ {254,10}, {254,100}, {1,1},    {72,1},    {6,1},     {96,1} },
{ {254,720},{254,7200},{1,72},   {1, 1},    {1,12},    {4, 3} },
{ {254,120},{254,1200},{1,12},   {6, 1},    {1,1},     {16,1} },
{ {254,960},{254,9600},{1,96},   {3, 4},    {1,16},    {1,1}  },

};
	
struct named named_convert(struct named n, int unit)
{
	struct named result;
	int old_unit;

	old_unit = n.unit;

	result = n;
	result.unit = unit;

	result.value *= convert[old_unit][unit].mul;
	result.value /= convert[old_unit][unit].div;

	return result;
}

int named_scan(struct named *pdest, char *str)
{
	double value;
	char unit[3] = {0};
	int unit_val = 0;

	int ret;

	ret = sscanf(str, "%f%2s", &value, unit);
	if (ret != 2)
		return 0;

	if (strcmp(str, "mm") == 0)
		unit_val = UNIT_MM;
	else if (strcmp(str, "cm") == 0)
		unit_val = UNIT_CM;
	else if (strcmp(str, "in") == 0)
		unit_val = UNIT_IN;
	else if (strcmp(str, "pt") == 0)
		unit_val = UNIT_PT;
	else if (strcmp(str, "pc") == 0)
		unit_val = UNIT_PC;
	else if (strcmp(str, "px") == 0)
		unit_val = UNIT_PX;
	else
		return 0; /* Incorrect unit */

	*pdest = named(value, unit_val);
	return 1;
}

struct named named_add(struct named a, struct named b)
{
	struct named result, bc;

	result = a;
	bc = named_convert(b, a.unit);

	result.value += bc.value;
	return result;
}

struct named named_sub(struct named a, struct named b)
{
	struct named result, bc;

	result = a;
	bc = named_convert(b, a.unit);

	result.value -= bc.value;
	return result;
}

struct named named_neg(struct named x)
{
	struct named result;

	result = x;
	result.value = -result.value;

	return result;
}

struct named named_constrain(struct named x, struct named a, struct named b)
{
	struct named ac, bc, result;

	result = x;
	ac = named_convert(a, x.unit);
	bc = named_convert(b, x.unit);

	if (result.value >= bc.value)
		result.value = bc.value;
	else if (result.value <= ac.value)
		result.value = ac.value;

	return result;
}

static char *names[] = { "mm", "cm", "in", "pt", "pc", "px" };

void named_string(char *dest, struct named src)
{
	double value;

	switch (src.unit) {
	case UNIT_MM:
		value = (double)src.value / NM_MILLIMETER;
		break;
	case UNIT_CM:
		value = (double)src.value / NM_CENTIMETER;
		break;
	case UNIT_IN:
		value = (double)src.value / NM_INCH;
		break;
	case UNIT_PT:
		value = (double)src.value / NM_POINT;
		break;
	case UNIT_PC:
		value = (double)src.value / NM_PICA;
		break;
	case UNIT_PX:
		value = (double)src.value / NM_PIXEL;
		break;
	}

	sprintf(dest, "%f%s", value, names[src.unit]);
}

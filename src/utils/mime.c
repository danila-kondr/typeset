/**
 *  This file is a part of odtgen, program which converts MD to ODT.
 *  Copyright (C) 2022  Danila Kondratenko <dan.kondratenko2013@ya.ru>
 *
 *  odtgen is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  odtgen is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <utils/mime.h>

#include <stddef.h>

static char mime_strs[][256] = {
	"application/octet-stream",

	"image/png",
	"image/jpeg",
	"image/gif",
	"image/svg+xml",
	"image/bmp",
	"image/x-eps",

	"math/mml",	
	"text/plain",
	"text/html",
	"",
};

struct extension {
	char ext[16];
	int mime_type;
};

static struct extension mime_table[] = {
	{ "png",         IMAGE_PNG },
	{ "jpg",         IMAGE_JPEG },
	{ "jpeg",        IMAGE_JPEG },
	{ "gif",         IMAGE_GIF },
	{ "svg",         IMAGE_SVG_XML },
	{ "bmp",         IMAGE_BMP },
	{ "dib",         IMAGE_BMP },
	{ "rle",         IMAGE_BMP },
	{ "eps",         IMAGE_X_EPS },
	{ "mml",         MATH_MML },

	{ "txt",         TEXT_PLAIN },
	{ "htm",         TEXT_HTML },
	{ "html",        TEXT_HTML },
	
	{ "",            APPLICATION_OCTET_STREAM },
};

char *mime_string(int type)
{
	if (type < N_MIME_TYPES)
		return mime_strs[type];
	return mime_strs[0];
} 

int mime_type_by_ext(char *ext)
{
	int i;

	for (i = 0; mime_table[i].ext[0]; i++) {
		if (strcmp(ext, mime_table[i].ext) == 0)
			return mime_table[i].mime_type;
	}
	return mime_table[i].mime_type;
}

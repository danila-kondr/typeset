/**
 *  This file is a part of odtgen, program which converts MD to ODT.
 *  Copyright (C) 2022  Danila Kondratenko <dan.kondratenko2013@ya.ru>
 *
 *  odtgen is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  odtgen is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <utils/xalloc.h>

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

static void fail() {
	perror("fatal");
	exit(1);
}

void *xmalloc(size_t n)
{
	void *result;

	assert(n > 0);
	
	result = malloc(n);
	if (!result)
		fail();

	return result;
}

void *xrealloc(void *op, size_t n)
{
	void *result;

	assert(n > 0);

	result = realloc(op, n);
	if (!result)
		fail();

	return result;
}

void xfree(void *p)
{
	if (p)
		free(p);
}

void *xcalloc(size_t nmemb, size_t size)
{
	void *result;

	assert(nmemb > 0 && size > 0);

	result = calloc(nmemb, size);
	if (!result)
		fail();

	return result;
}

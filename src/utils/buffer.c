/**
 *  This file is a part of odtgen, program which converts MD to ODT.
 *  Copyright (C) 2022  Danila Kondratenko <dan.kondratenko2013@ya.ru>
 *
 *  odtgen is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  odtgen is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <utils/buffer.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdarg.h>

#include <b64.h>

#include <utils/error.h>
#include <utils/xalloc.h>

static size_t next_power(size_t x)
{
	size_t mask = 1;
	
	while (mask <= x)
		mask <<= 1;
	return mask;
}

struct buffer *buffer_new()
{
	struct buffer *result;
	result = xcalloc(1, sizeof(struct buffer));
	return result;
}

void buffer_delete(struct buffer **pBuf)
{
	struct buffer *buf = *pBuf;

	if (!buf)
		return;

	xfree((*pBuf)->b);
	xfree(*pBuf);
	*pBuf = NULL;
}

void buffer_put_byte(struct buffer *buf, char b)
{
	assert(buf);

	if (!buf->b) {
		buf->size = 16;
		buf->b = xmalloc(buf->size);
		buf->pos = 0;
	}

	if (buf->pos + 1 > buf->size) {
		buf->size = next_power(buf->size + 1);
		buf->b = xrealloc(buf->b, buf->size);
	}

	buf->b[buf->pos++] = b;
} 

void buffer_put_u8char(struct buffer *buf, int32_t c)
{
	if (c >= 0x000000 && c <= 0x00007f) {
		buffer_put_byte(buf, (uint8_t)c & 0xFF);
	}
	else if (c >= 0x000080 && c <= 0x0007ff) {
		buffer_put_byte(buf, 0xC0 | ((c & 0x07c0) >> 6));
		buffer_put_byte(buf, 0x80 | (c & 0x003f));
	}
	else if (c >= 0x000800 && c <= 0x00ffff) {
		buffer_put_byte(buf, 0xE0 | ((c & 0xf000) >> 12));
		buffer_put_byte(buf, 0x80 | ((c & 0x0fc0) >>  6));
		buffer_put_byte(buf, 0x80 |  (c & 0x003f));
	}
	else if (c >= 0x010000 && c <= 0x10ffff) {
		buffer_put_byte(buf, 0xF0 | ((c & 0x1c0000) >> 18));
		buffer_put_byte(buf, 0x80 | ((c & 0x03f000) >> 12));
		buffer_put_byte(buf, 0x80 | ((c & 0x000fc0) >>  6));
		buffer_put_byte(buf, 0x80 |  (c & 0x00003f));
	}
}

void buffer_put(struct buffer *buf, uint8_t *x, size_t n)
{
	size_t i;
	assert(buf);

	if (!buf->b) {
		buf->size = n + 1;
		buf->b = xmalloc(buf->size);
		buf->pos = 0;
	}

	if (buf->pos + n > buf->size) {
		buf->size = next_power(buf->size + n);
		buf->b = xrealloc(buf->b, buf->size);
	}

	for (i = 0; i < n; i++)
		buf->b[buf->pos++] = x[i];
}

void buffer_put_base64(struct buffer *buf, uint8_t *x, size_t n)
{
	char *b64;

	b64 = b64_encode(x, n);
	if (!b64)
		error(-1, "b64: failed to allocate memory");	

	buffer_put_string(buf, b64);
	free(b64);
}

void buffer_put_string(struct buffer *buf, char *x)
{
	buffer_put(buf, x, strlen(x));
}

void buffer_put_wstring(struct buffer *buf, wchar_t *x)
{
	int letter = 0;

	while (*x) {
		if (*x >= 0xD800 && *x <= 0xDBFF) {
			letter = (*x & 0x3ff) << 10;
			x++;
			if (!*x) {
				buffer_put_u8char(buf, 0xFFFD);
				break;
			}
			if (*x >= 0xDC00 && *x <= 0xDFFF) {
				letter = (letter | (*x & 0x3ff)) + 0x10000;
			}
			else {
				buffer_put_u8char(buf, 0xFFFD);
				continue;
			}
		}
		else {
			letter = *x;
		}
		buffer_put_u8char(buf, letter);
		x++;
	}
}

void buffer_printf(struct buffer *buf, const char *fmt, ...)
{
	va_list vl, vl_copy;
	char *formatted;
	size_t len;

	va_start(vl, fmt);
	va_copy(vl_copy, vl);

	len = vsnprintf(NULL, 0, fmt, vl_copy);
	formatted = xcalloc(len + 1, sizeof(char));
	vsnprintf(formatted, len + 1, fmt, vl);
	formatted[len] = '\0';

	va_end(vl_copy);
	va_end(vl);

	buffer_put_string(buf, formatted);
	xfree(formatted);
}

void buffer_clear(struct buffer *buf)
{
	xfree(buf->b);
	buf->b = NULL;
	buf->size = 0;
	buf->pos = 0;
}

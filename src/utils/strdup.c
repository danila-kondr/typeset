/**
 *  This file is a part of odtgen, program which converts MD to ODT.
 *  Copyright (C) 2022  Danila Kondratenko <dan.kondratenko2013@ya.ru>
 *
 *  odtgen is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  odtgen is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <utils/strdup.h>

#include <utils/xalloc.h>
#include <string.h>

char *xstrdup(char *x)
{
	size_t len = strlen(x);
	char *result;

	result = xcalloc(len + 1, sizeof(char));
	strncpy(result, x, len);

	return result;
}

char *xstrndup(char *x, size_t n_chars)
{
	size_t len = strlen(x);
	char *result;

	if (len >= n_chars)
		len = n_chars;

	result = xcalloc(len + 1, sizeof(char));
	strncpy(result, x, len);

	return result;
}

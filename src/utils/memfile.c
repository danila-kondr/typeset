/**
 *  This file is a part of odtgen, program which converts MD to ODT.
 *  Copyright (C) 2022  Danila Kondratenko <dan.kondratenko2013@ya.ru>
 *
 *  odtgen is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  odtgen is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <utils/memfile.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>

#include <utils/xalloc.h>
#include <utils/strdup.h>
#include <utils/mime.h>

static char *xstrlwr(char *x)
{
	char *p = x;

	while (*p) {
		*p = tolower(*p);
		p++;
	}
	return x;
}

struct memfile *memfile_new()
{
	struct memfile *result;
	result = xcalloc(1, sizeof(struct memfile));
	return result;
}

struct memfile *memfile_read(const char *filename)
{
	FILE *f;
	uint8_t chunk[4096];
	size_t n_read;
	struct memfile *file;
	char *ext;

	file = memfile_new();

	f = fopen(filename, "rb");
	if (!f) {
		perror(filename);
		exit(1);
	}

	do {
		n_read = fread(chunk, 1, 4096, f);
		memfile_put(file, chunk, n_read);
	} while (n_read == 4096);
	fclose(f);

	ext = strrchr(filename, '.');
	if (!ext) {
		file->type = APPLICATION_OCTET_STREAM;
	} else {
		ext++;
		ext = xstrlwr(xstrdup(ext));
		if (strcmp(ext, "png") == 0)
			file->type = IMAGE_PNG;
		else if (strcmp(ext, "jpg") == 0)
			file->type = IMAGE_JPEG;
		else if (strcmp(ext, "jpeg") == 0)
			file->type = IMAGE_JPEG;
		else if (strcmp(ext, "gif") == 0)
			file->type = IMAGE_GIF;
		else if (strcmp(ext, "svg") == 0)
			file->type = IMAGE_SVG_XML;
		else if (strcmp(ext, "bmp") == 0)
			file->type = IMAGE_BMP;
		else if (strcmp(ext, "txt") == 0)
			file->type = TEXT_PLAIN;
		else if (strcmp(ext, "htm") == 0)
			file->type = TEXT_HTML;
		else if (strcmp(ext, "html") == 0)
			file->type = TEXT_HTML;
		else if (strcmp(ext, "mml") == 0)
			file->type = MATH_MML;
		else
			file->type = APPLICATION_OCTET_STREAM;
		xfree(ext);
	}

	return file;
}

void memfile_write(struct memfile *file, const char *filename)
{
	FILE *o;

	o = fopen(filename, "wb");
	if (!o) {
		perror(filename);
		exit(1);
	}

	if (file)
		fwrite(BUFFER(file)->b, 1, BUFFER(file)->pos, o);
	fclose(o);
}

/**
 *  This result is a part of odtgen, program which converts MD to ODT.
 *  Copyright (C) 2022  Danila Kondratenko <dan.kondratenko2013@ya.ru>
 *
 *  odtgen is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  odtgen is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <utils/tex.h>
#include <utils/mime.h>
#include <utils/error.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <time.h>

/* The author of odtgen doesn't know at all how to write a decent
 * LaTeX to MathML translator so the Don Knuth's TeX engine will be
 * used.
 */
/* PS: It is impossible to create decent LaTeX to MathML translator
 * because LibreOffice cannot render MathML natively. It must convert
 * it into StarMath, then it must convert it into its own AST and only
 * then render it.
 * StarMath parser demands operators to have operands to the left and
 * right to them, and otherwise it tells about error and stops rendering
 * the formula.
 * I need to create my own mathematical notation which can be converted
 * to MathML one because I can't write StarMath parser. Period.
 */
struct memfile *tex_render_formula(char *formula, size_t size, int dpi,
	int mode)
{
	FILE *tex;
	char texname[15] = "texXXXXXX";
	char outname[16];
	char cmdline[256];
	struct memfile *result;
	int i, r;

	if (dpi == 0)
		dpi = TEX_DEFAULT_DPI;

	srand(time(NULL));
	for (i = 0; texname[i]; i++) {
		if (texname[i] == 'X')
			texname[i] = ((r=rand()%52) >= 26) 
				? ('a'+r-26) 
				: ('A'+r);
	}

	tex = fopen(texname, "wt");
	if (!tex) {
		perror(texname);
		exit(1);
	}

	fputs("\\documentclass[varwidth]{standalone}\n"
              "\\usepackage{amsmath,amssymb,amsfonts}\n"
              "\\usepackage[utf8]{inputenc}\n"
	      "\\begin{document}\n"
	      "\\LARGE\n"
	      "$", tex);
	if (mode == TEX_DISPLAY_MODE)
		fputs("$", tex);
	fwrite(formula, 1, size, tex);
	if (mode == TEX_DISPLAY_MODE)
		fputs("$", tex);
	fputs("$\n"
	      "\\end{document}\n", tex);

	fclose(tex);

	sprintf(cmdline, "latex %s", texname);
	r = system(cmdline);
	if (r == -1)
		error(1, "latex: %s", strerror(errno));

	sprintf(cmdline, "dvisvgm -n -o%s.svg %s.dvi", texname, texname);
	r = system(cmdline);
	if (r == -1)
		error(1, "latex: %s", strerror(errno));

	sprintf(outname, "%s.svg", texname);
	result = memfile_read(outname);
	result->type = IMAGE_SVG_XML;
	remove(outname);

	sprintf(outname, "%s.aux", texname); remove(outname);
	sprintf(outname, "%s.dvi", texname); remove(outname);
	sprintf(outname, "%s.log", texname); remove(outname);
	remove(texname);

	return result;
}

/**
 *  This file is a part of odtgen, program which converts MD to ODT.
 *  Copyright (C) 2022  Danila Kondratenko <dan.kondratenko2013@ya.ru>
 *
 *  odtgen is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  odtgen is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <renderer.h>

#include <string.h>
#include <unistd.h>

#include <utils/buffer.h>
#include <utils/error.h>
#include <utils/mime.h>
#include <utils/readtext.h>
#include <utils/strdup.h>
#include <utils/xalloc.h>

#include <cwalk.h>

static char *input_dirname(char *x);

void renderer_init(struct renderer *renderer, enum document_type type)
{
	memset(&renderer->parser, 0, sizeof(renderer->parser));

	renderer->document_type = type;

	renderer->parser.flags = MD_FLAG_TABLES
		| MD_FLAG_NOHTMLBLOCKS
		| MD_FLAG_NOHTMLSPANS;
	if (renderer->flags & PROGRAM_FLAG_FORMULAS)
		renderer->parser.flags |= MD_FLAG_LATEXMATHSPANS;

	renderer->header = buffer_new();
	renderer->styles = buffer_new();
	renderer->content = buffer_new();
	renderer->footer = buffer_new();

	renderer->n_images = 0;
	sh_new_strdup(renderer->resources);

	switch (type) {
	case DOCUMENT_ODT:
	case DOCUMENT_FLAT_ODT:
		odt_init(renderer);
		break;
	default:
		error(-1, "Invalid document type");
	}

	renderer->file = NULL;
	renderer->filename = NULL;
	renderer->input_dirname = NULL;
	renderer->blocks = NULL;
	renderer->spans = NULL;
}

int render(struct renderer *renderer, char *input_filename,
		char *output_filename)
{
	struct buffer *data;
	int ret;

	if (renderer->flags & PROGRAM_FLAG_VERBOSE)
		fprintf(stderr, "Reading input file %s\n", input_filename);

	data = read_text(input_filename);
	if (data->pos == 0)
		return 0;

	if (strcmp(output_filename, "-") == 0)
		renderer->file = stdout;
	else
		renderer->file = fopen(output_filename, "wb");

	renderer->input_dirname = input_dirname(input_filename);
	renderer->formula = buffer_new();

	md_parse((MD_CHAR *)data->b, 
		data->pos, 
		&renderer->parser, 
		(void *)renderer
	);

	buffer_delete(&data);
	buffer_delete(&renderer->formula);
	xfree(renderer->input_dirname);

	renderer->write(renderer);
	if (renderer->header)    buffer_delete(&renderer->header);
	if (renderer->styles)    buffer_delete(&renderer->styles);
	if (renderer->content)   buffer_delete(&renderer->content);
	if (renderer->footer)    buffer_delete(&renderer->footer);

	if (renderer->resources) hmfree(renderer->resources);

	fclose(renderer->file);
}

static char *input_dirname(char *x)
{
	char cwd[1024], absolute[1024];
	size_t dirname_len;
	char *result;

	getcwd(cwd, 1024);
	cwk_path_get_absolute(cwd, x, absolute, 1024);
	cwk_path_get_dirname(absolute, &dirname_len);

	if (dirname_len == 0)
		return xstrdup(".");

	result = xcalloc(dirname_len + 1, sizeof(char));
	strncpy(result, absolute, dirname_len);

	return result;
}

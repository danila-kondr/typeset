/**
 *  This file is a part of odtgen, program which converts MD to ODT.
 *  Copyright (C) 2022  Danila Kondratenko <dan.kondratenko2013@ya.ru>
 *
 *  odtgen is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  odtgen is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "rdf.h"

#define XML_SPEC "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"

#define RDF_BEGIN \
	"<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">"

#define ODF_RESOURCE "http://docs.oasis-open.org/ns/office/1.2/meta/odf"
#define ODF_PKG_RESOURCE "http://docs.oasis-open.org/ns/office/1.2/meta/pkg#"

#define RDF_DESCRIPTION(_about, _resource) \
	"<rdf:Description rdf:about=\"" _about "\">" \
	"<rdf:type rdf:resource=\"" _resource "\" />" \
	"</rdf:Description>"

#define RDF_DESCRIPTION_PKG(_x) \
	"<rdf:Description rdf:about=\"\">" \
	"<ns0:hasPart xmlns:ns0=\"" ODF_PKG_RESOURCE "\" " \
	"rdf:resource=\"" _x "\" />" \
	"</rdf:Description>"

#define RDF_END "</rdf:RDF>"

char manifest_rdf[] =
	XML_SPEC
	RDF_BEGIN
	RDF_DESCRIPTION("styles.xml", ODF_RESOURCE "#StylesFile")
	RDF_DESCRIPTION_PKG("styles.xml")
	RDF_DESCRIPTION("content.xml", ODF_RESOURCE "#ContentFile")
	RDF_DESCRIPTION_PKG("content.xml")
	RDF_DESCRIPTION("", ODF_RESOURCE "#Document")
	RDF_END
	;

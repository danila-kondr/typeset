Примеры кода
============

Простейший код на Си:

```
#include <stdio.h>

int main(void)
{	printf("Hello, world!\n");
	return 0;
}
```

Код посложнее.

```
#include <stdio.h>
#include <stdlib.h>

int cmp(const void *p1, const void *p2)
{	return *(int*)p1 - *(int*)p2;
}

int main(void)
{	int *A, N, i;

	srand(1);
	printf("Vvedite N: ");
	scanf("%d", &N);
	A = malloc(N * sizeof(int));

	for (i = 0; i < N; i++)
		A[i] = rand() % 256;
	qsort(A, N, sizeof(int), cmp);

	for (i = 0; i < N; i++)
		printf("%d ", A[i]);
	printf("\n");

	return 0;
}
```

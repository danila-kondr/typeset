# Проверка изображения

![Изображение](test.png)

![Роза](rose.png)

![Роза в формате GIF](rose.gif)

## Проверка вставки рисунков

~~~figure
test.png
~~~
Рисунок 1. Красный фон

~~~figure
rose.png
~~~
Рисунок 2. Роза (ImageMagick)

~~~figure
texput.eps
~~~
Рисунок 3. Пример текста, сгенерированного LaTeX

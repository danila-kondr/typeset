/**
 *  This file is a part of odtgen, program which converts MD to ODT.
 *  Copyright (C) 2022  Danila Kondratenko <dan.kondratenko2013@ya.ru>
 *
 *  odtgen is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  odtgen is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Interface to the latex binary. */
#ifndef _UTILS_TEX_H_
#define _UTILS_TEX_H_

#include <utils/memfile.h>

#define TEX_DEFAULT_DPI 144

enum render_mode {
	TEX_TEXT_MODE,
	TEX_DISPLAY_MODE,
};

struct memfile *tex_render_formula(char *formula, size_t size, int dpi, 
	int mode);

#endif

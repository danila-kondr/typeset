/**
 *  This file is a part of odtgen, program which converts MD to ODT.
 *  Copyright (C) 2022  Danila Kondratenko <dan.kondratenko2013@ya.ru>
 *
 *  odtgen is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  odtgen is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _MEMFILE_H_
#define _MEMFILE_H_

#include <stddef.h>
#include <stdint.h>

#include <utils/buffer.h>

struct memfile {
	struct buffer buf;	
	int type;
};

#define BUFFER(_mf) ( (struct buffer *)(_mf) )

struct memfile *memfile_new();

struct memfile *memfile_read(const char *filename);
void memfile_write(struct memfile *file, const char *filename);

#define memfile_delete(x) buffer_delete((struct buffer **)(x))
#define memfile_put(x, ...) buffer_put(BUFFER(x), __VA_ARGS__)
#define memfile_put_byte(x, ...) buffer_put_byte(BUFFER(x), __VA_ARGS__)
#define memfile_put_string(x, ...) buffer_put_string(BUFFER(x), __VA_ARGS__)

#endif

/**
 *  This file is a part of odtgen, program which converts MD to ODT.
 *  Copyright (C) 2022  Danila Kondratenko <dan.kondratenko2013@ya.ru>
 *
 *  odtgen is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  odtgen is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _MIME_H_
#define _MIME_H_

enum mime_type {
	APPLICATION_OCTET_STREAM,

	IMAGE_PNG,
	IMAGE_JPEG,
	IMAGE_GIF,
	IMAGE_SVG_XML,
	IMAGE_BMP,
	IMAGE_X_EPS,

	MATH_MML,
	TEXT_PLAIN,
	TEXT_HTML,

	N_MIME_TYPES
};

char *mime_string(int type);
int mime_type_by_ext(char *ext);

#endif

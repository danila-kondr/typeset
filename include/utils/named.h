/**
 *  This file is a part of odtgen, program which converts MD to ODT.
 *  Copyright (C) 2022  Danila Kondratenko <dan.kondratenko2013@ya.ru>
 *
 *  odtgen is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  odtgen is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _NAMED_H_
#define _NAMED_H_

#define NM_PIXEL        49152L
#define NM_POINT        65536L
#define NM_PICA        786432L
#define NM_INCH       4718592L
#define NM_MILLIMETER  185772L
#define NM_CENTIMETER 1857713L

enum unit {
	UNIT_MM,
	UNIT_CM,
	UNIT_IN,
	UNIT_PT,
	UNIT_PC,
	UNIT_PX,
};

struct named {
	long value;
	int unit;
};

struct named named(double value, int unit);
struct named named_convert(struct named n, int unit);

int named_scan(struct named *pdest, char *str);

struct named named_add(struct named a, struct named b);
struct named named_sub(struct named a, struct named b);
struct named named_neg(struct named x);

struct named named_constrain(struct named x, struct named a, struct named b);

void named_string(char *dest, struct named src);

#endif

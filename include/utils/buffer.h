/**
 *  This file is a part of odtgen, program which converts MD to ODT.
 *  Copyright (C) 2022  Danila Kondratenko <dan.kondratenko2013@ya.ru>
 *
 *  odtgen is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  odtgen is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _BUFFER_H_
#define _BUFFER_H_

#include <stddef.h>
#include <stdint.h>

#include <wchar.h>

struct buffer {
	uint8_t *b;
	size_t size;
	size_t pos;
};

struct buffer *buffer_new();
void buffer_delete(struct buffer **pBuf);

void buffer_put_byte(struct buffer *buf, char byte);
void buffer_put_u8char(struct buffer *buf, int32_t c);
void buffer_put(struct buffer *buf, uint8_t *x, size_t n);
void buffer_put_base64(struct buffer *buf, uint8_t *x, size_t n);
void buffer_put_string(struct buffer *buf, char *x);
void buffer_put_wstring(struct buffer *buf, wchar_t *x);

void buffer_printf(struct buffer *buf, const char *fmt, ...);

void buffer_clear(struct buffer *buf);

#endif

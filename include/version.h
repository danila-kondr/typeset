#define _ODTGEN_MAJOR 0
#define _ODTGEN_MINOR 4
#define _ODTGEN_PATCH 0

#define __XN(_x) #_x
#define __N(_x) __XN(_x)
#define __ODTGEN_VER_STR(_M, _m, _p) __N(_M) "." __N(_m) "." __N(_p) "-alpha.1"

#define _ODTGEN_VER_STR \
	__ODTGEN_VER_STR(_ODTGEN_MAJOR, _ODTGEN_MINOR, _ODTGEN_PATCH)

#ifndef _RENDERER_H_
#define _RENDERER_H_

#include <md4c.h>

#include <stb_ds.h>
#include <utils/buffer.h>
#include <utils/memfile.h>
#include <utils/named.h>

struct size {
	struct named width;
	struct named height;
};

struct margins {
	struct named left;
	struct named right;
	struct named top;
	struct named bottom;
};

struct resource {
	char *key;
	struct memfile *value;
};

enum document_type {
	DOCUMENT_ODT,
	DOCUMENT_FLAT_ODT,
};

enum formula_type {
	FORMULA_NOTHING,
	FORMULA_TEXT,
	FORMULA_DISPLAY,
};

#define ODT_NUMBERING_123 "1"
#define ODT_NUMBERING_CYRILLIC "а, б, .., аа, аб, ... (ru)"

struct list_style {
	char numbering_name[256];
	struct named text_indent;
	struct named margin_left;
};

#define PROGRAM_FLAG_VERBOSE       0x00000001UL
#define PROGRAM_FLAG_FORMULAS      0x00000002UL
#define ODT_FLAG_FIRST_PAGE_NUMBER 0x00010000UL
#define ODT_FLAG_OUTLINE_NUMBER    0x00020000UL

#define BLOCK_LIST_ITEM_RAW 0x100
#define SPAN_IMAGE_ALT      0x101
struct renderer {
	MD_PARSER parser;
	enum document_type document_type;
	unsigned long flags;
	/* Document buffers */
	struct buffer *header;
	struct buffer *styles;
	struct buffer *content;
	struct buffer *footer;

	FILE *file;
	char *filename;
	char *input_dirname;

	/* Stacks for blocks and spans. */
	int *blocks;
	int *spans;

	/* Formulas */
	enum formula_type formula_type;
	struct buffer *formula;

	/* Code fences */
	int fence_type;
	char *fence_text;
	struct buffer *fence;

	/* Document properties */
	struct named font_size;
	struct named modern_font_size;
	struct size paper_size;
	struct margins margins;
	struct named text_indent;
	int line_height;

	struct size text_size;
	struct named footer_center_tab_stop;
	struct named footer_right_tab_stop;

	struct list_style list_styles[10];

	/* Inner resources */
	int n_images;
	struct resource *resources;

	void (*write)(struct renderer *renderer);
};

void renderer_init(struct renderer *renderer, enum document_type type);
int render(struct renderer *renderer, char *input_filename, 
		char *output_filename);

void odt_init(struct renderer *renderer);

#endif

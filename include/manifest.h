/**
 *  This file is a part of odtgen, program which converts MD to ODT.
 *  Copyright (C) 2022  Danila Kondratenko <dan.kondratenko2013@ya.ru>
 *
 *  odtgen is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  odtgen is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _MANIFEST_H_
#define _MANIFEST_H_

#include <utils/buffer.h>

struct odf_manifest_node {
	char *name;
	char *mimetype;
	struct odf_manifest_node *next;
};

struct odf_manifest {
	struct odf_manifest_node *head;
	struct odf_manifest_node *tail;
};

struct odf_manifest *odf_manifest_new();
void odf_manifest_delete(struct odf_manifest **pmanifest);

void odf_manifest_append(struct odf_manifest *manifest, 
	char *name, char *mimetype);

struct buffer *odf_manifest_xml(struct odf_manifest *manifest);

#endif

/**
 *  This file is a part of odtgen, program which converts MD to ODT.
 *  Copyright (C) 2022  Danila Kondratenko <dan.kondratenko2013@ya.ru>
 *
 *  odtgen is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  odtgen is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _ODT_TEMPLATE_H_
#define _ODT_TEMPLATE_H_

/* Document preambles */

#define ODT_MIMETYPE "application/vnd.oasis.opendocument.text"

#define XMLNS(_namespace, _uri) "\n    xmlns:" #_namespace "=\"" _uri "\""

#define OPENDOCUMENT_URN(_name, _version) \
	"urn:oasis:names:tc:opendocument:xmlns:" _name ":" _version

#define XML_SPEC "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"

#define XML_NAMESPACES                                                \
	XMLNS(office, OPENDOCUMENT_URN("office", "1.0"))              \
	XMLNS(css3t, "http://www.w3.org/TR/css3-text")                \
	XMLNS(chart, OPENDOCUMENT_URN("chart", "1.0"))                \
	XMLNS(svg, OPENDOCUMENT_URN("svg-compatible", "1.0"))         \
	XMLNS(text, OPENDOCUMENT_URN("text", "1.0"))                  \
	XMLNS(style, OPENDOCUMENT_URN("style", "1.0"))                \
	XMLNS(meta, OPENDOCUMENT_URN("meta", "1.0"))                  \
	XMLNS(dc, "http://purl.org/dc/elements/1.1/")                 \
	XMLNS(draw, OPENDOCUMENT_URN("drawing", "1.0"))               \
	XMLNS(xlink, "http://www.w3.org/1999/xlink")                  \
	XMLNS(fo, OPENDOCUMENT_URN("xsl-fo-compatible", "1.0"))       \
	XMLNS(table, OPENDOCUMENT_URN("table", "1.0"))                \
	XMLNS(number, OPENDOCUMENT_URN("datastyle", "1.0"))           \
	XMLNS(of, OPENDOCUMENT_URN("of", "1.2"))                      \
	XMLNS(math, "http://www.w3.org/1998/Math/MathML")             \
	XMLNS(xsd, "http://www.w3.org/2001/XMLSchema")                \
	XMLNS(xsi, "http://www.w3.org/2001/XMLSchema-instance")       \
	XMLNS(xhtml, "http://www.w3.org/1999/xhtml") 

#define FLAT_ODT_DOCUMENT_BEGIN "<office:document\n"                  \
	"    office:mimetype=\"" ODT_MIMETYPE "\"\n"                  \
	"    office:version=\"1.3\""                                  \
	XML_NAMESPACES ">\n"

#define FLAT_ODT_DOCUMENT_END "</office:document>"

#define ODT_DOCUMENT_STYLES_BEGIN "<office:document-styles\n"         \
	"    office:version=\"1.3\""                                  \
	XML_NAMESPACES ">\n"

#define ODT_DOCUMENT_STYLES_END "</office:document-styles>"

#define ODT_DOCUMENT_CONTENT_BEGIN "<office:document-content\n"       \
	"    office:version=\"1.3\""                                  \
	XML_NAMESPACES ">\n"

#define ODT_DOCUMENT_CONTENT_END "</office:document-content>"

/* Default template for document */

#define ODT_FONT_FACE_DECLS                                            \
	"<office:font-face-decls>\n"                                   \
	"<style:font-face\n"                                           \
	"    style:name=\"{{RomanFont}}\"\n"                           \
	"    svg:font-family=\"&apos;{{RomanFont}}&apos;\"\n"          \
	"    style:font-family-generic=\"roman\"/>\n"                  \
	"<style:font-face\n"                                           \
	"    style:name=\"{{ModernFont}}\"\n"                          \
	"    svg:font-family=\"&apos;{{ModernFont}}&apos;\"\n"         \
	"    style:font-family-generic=\"modern\"/>\n"                 \
	"<style:font-face\n"                                           \
	"    style:name=\"OpenSymbol\"\n"                              \
	"    svg:font-family=\"&apos;OpenSymbol&apos;\"\n"             \
	"    style:font-charset=\"x-symbol\"/>\n"                      \
	"</office:font-face-decls>\n"

#define ODT_DEFAULT_STYLE_PARAGRAPH                                    \
	"<style:default-style style:family=\"paragraph\">\n"           \
	"<style:text-properties\n"                                     \
	"    style:font-name=\"{{RomanFont}}\"\n"                      \
	"    fo:font-size=\"{{RomanFontSize}}\"\n"                     \
	"    fo:font-weight=\"normal\"/>\n"                            \
	"</style:default-style>"

#define ODT_STYLE_STANDARD                                             \
	"<style:style style:name=\"Standard\"\n"                       \
	"    style:family=\"paragraph\"\n"                             \
	"    style:class=\"text\">\n"                                  \
	"<style:paragraph-properties style:page-number=\"auto\"/>\n"   \
	"</style:style>\n"

#define ODT_STYLE_HEADING                                              \
	"<style:style style:name=\"Heading\"\n"                        \
	"    style:family=\"paragraph\"\n"                             \
	"    style:class=\"text\"\n"                                   \
	"    style:parent-style-name=\"Standard\">\n"                  \
	"<style:paragraph-properties\n"                                \
	"    fo:line-height=\"{{LineHeight}}\"\n"                      \
	"    fo:text-indent=\"{{TextIndent}}\"/>\n"                    \
	"<style:text-properties\n"                                     \
	"    style:font-name=\"{{RomanFont}}\"\n"                      \
	"    fo:font-size=\"{{RomanFontSize}}\"\n"                     \
	"    fo:font-weight=\"normal\"\n"                              \
	"    fo:font-style=\"normal\"/>\n"                             \
	"</style:style>\n"

#define ODT_STYLE_TEXT_BODY                                            \
	"<style:style style:name=\"Text_20_body\"\n"                   \
	"    style:display-name=\"Text body\"\n"                       \
	"    style:family=\"paragraph\"\n"                             \
	"    style:class=\"text\"\n"                                   \
	"    style:parent-style-name=\"Standard\">\n"                  \
	"<style:paragraph-properties\n"                                \
	"    fo:line-height=\"{{LineHeight}}\"\n"                      \
	"    fo:text-indent=\"{{TextIndent}}\"\n"                      \
	"    fo:text-align=\"{{TextAlign}}\"/>\n"                      \
	"</style:style>\n"

#define ODT_STYLE_PREFORMATTED_TEXT                                    \
	"<style:style style:name=\"Preformatted_20_Text\"\n"           \
	"    style:display-name=\"Preformatted Text\"\n"               \
	"    style:family=\"paragraph\"\n"                             \
	"    style:class=\"html\"\n"                                   \
	"    style:parent-style-name=\"Standard\">\n"                  \
	"<style:paragraph-properties\n"                                \
	"    fo:margin-bottom=\"6pt\"/>\n"                             \
	"<style:text-properties\n"                                     \
	"    style:font-name=\"{{ModernFont}}\"\n"                     \
	"    fo:font-size=\"{{ModernFontSize}}\"/>\n"                  \
	"</style:style>\n"

#define ODT_STYLE_TABLE_CONTENTS                                       \
	"<style:style style:name=\"Table_20_Contents\"\n"              \
	"    style:display-name=\"Table Contents\"\n"                  \
	"    style:family=\"paragraph\"\n"                             \
	"    style:class=\"text\"\n"                                   \
	"    style:parent-style-name=\"Standard\">\n"                  \
	"<style:paragraph-properties\n"                                \
	"    fo:text-align=\"left\"/>\n"                               \
	"</style:style>\n"

#define ODT_STYLE_TABLE_HEADING                                        \
	"<style:style style:name=\"Table_20_Heading\"\n"               \
	"    style:display-name=\"Table Heading\"\n"                   \
	"    style:family=\"paragraph\"\n"                             \
	"    style:class=\"text\"\n"                                   \
	"    style:parent-style-name=\"Table_20_Contents\">\n"         \
	"<style:paragraph-properties\n"                                \
	"    fo:text-align=\"center\"/>\n"                             \
	"<style:text-properties\n"                                     \
	"    fo:font-weight=\"bold\"\n"                                \
	"    fo:font-style=\"normal\"/>\n"                             \
	"</style:style>\n"

#define ODT_STYLE_HEADING_n_NAME(_n) "Heading_20_" #_n
#define ODT_STYLE_HEADING_n_DISPLAY_NAME(_n) "Heading " #_n

#define ODT_STYLE_HEADING_n_BREAK(_n, _weight, _style)                 \
	"<style:style style:name=\"" ODT_STYLE_HEADING_n_NAME(_n)      \
	"\"\n"                                                         \
	"    style:display-name=\"" ODT_STYLE_HEADING_n_DISPLAY_NAME(_n)\
	"\"\n"                                                         \
	"    style:family=\"paragraph\"\n"                             \
	"    style:class=\"text\"\n"                                   \
	"    style:parent-style-name=\"Heading\"\n"                    \
	"    style:next-style-name=\"Text_20_body\"\n"                 \
	"    style:default-outline-level=\"" #_n "\">\n"               \
	"<style:paragraph-properties\n"                                \
	"    fo:line-height=\"{{LineHeight}}\"\n"                      \
	"    fo:text-indent=\"{{TextIndent}}\"\n"                      \
	"    fo:break-before=\"page\"/>\n"                             \
	"<style:text-properties\n"                                     \
	"    fo:font-weight=\"" _weight "\"\n"                         \
	"    fo:font-style=\"" _style "\"/>\n"                         \
	"</style:style>\n"

#define ODT_STYLE_HEADING_n(_n, _weight, _style)                       \
	"<style:style style:name=\"" ODT_STYLE_HEADING_n_NAME(_n)      \
	"\"\n"                                                         \
	"    style:display-name=\"" ODT_STYLE_HEADING_n_DISPLAY_NAME(_n)\
	"\"\n"                                                         \
	"    style:family=\"paragraph\"\n"                             \
	"    style:class=\"text\"\n"                                   \
	"    style:parent-style-name=\"Heading\"\n"                    \
	"    style:next-style-name=\"Text_20_body\"\n"                 \
	"    style:default-outline-level=\"" #_n "\">\n"               \
	"<style:paragraph-properties\n"                                \
	"    fo:line-height=\"{{LineHeight}}\"\n"                      \
	"    fo:text-indent=\"{{TextIndent}}\"/>\n"                    \
	"<style:text-properties\n"                                     \
	"    fo:font-weight=\"" _weight "\"\n"                         \
	"    fo:font-style=\"" _style "\"/>\n"                         \
	"</style:style>\n"

#define ODT_OUTLINE_LEVEL_STYLE_SPACE(_n)                              \
	"<text:outline-level-style text:level=\"" #_n "\"\n"           \
	"    style:num-format=\"1\"\n"                                 \
	"    text:display-levels=\"" #_n "\">\n"                       \
	"<style:list-level-properties\n"                               \
	"    text:list-level-position-and-space-mode="                 \
	"\"label-alignment\">\n"                                       \
	"<style:list-level-label-alignment\n"                          \
	"    text:label-followed-by=\"space\"/>\n"                     \
	"</style:list-level-properties>\n"                             \
	"</text:outline-level-style>\n"

#define ODT_OUTLINE_STYLE_OUTLINE                                     \
	"<text:outline-style style:name=\"Outline\">\n"               \
	ODT_OUTLINE_LEVEL_STYLE_SPACE(1)                              \
	ODT_OUTLINE_LEVEL_STYLE_SPACE(2)                              \
	ODT_OUTLINE_LEVEL_STYLE_SPACE(3)                              \
	ODT_OUTLINE_LEVEL_STYLE_SPACE(4)                              \
	ODT_OUTLINE_LEVEL_STYLE_SPACE(5)                              \
	ODT_OUTLINE_LEVEL_STYLE_SPACE(6)                              \
	ODT_OUTLINE_LEVEL_STYLE_SPACE(7)                              \
	ODT_OUTLINE_LEVEL_STYLE_SPACE(8)                              \
	ODT_OUTLINE_LEVEL_STYLE_SPACE(9)                              \
	ODT_OUTLINE_LEVEL_STYLE_SPACE(10)                             \
	"</text:outline-style>\n"

#define ODT_LIST_STYLE_BEGIN(_name, _display_name)                    \
	"<text:list-style style:name=\"" _name                        \
	"\" style:display-name=\"" _display_name "\">\n" 

#define ODT_LIST_STYLE_END "</text:list-style>"

#define ODT_LIST_LEVEL_STYLE_NUM(_l, _s, _f, _i, _ml)                 \
	"<text:list-level-style-number text:level=\"" _l "\"\n"       \
	"    style:num-suffix=\"" _s "\"\n"                           \
	"    style:num-format=\"" _f "\">\n"                          \
	"<style:list-level-properties\n"                              \
	"    text:list-level-position-and-"                           \
	"space-mode=\"label-alignment\">\n"                           \
	"<style:list-level-label-alignment\n"                         \
	"    text:label-followed-by=\"space\"\n"                      \
	"    fo:text-indent=\"" _i "\"\n"                             \
	"    fo:margin-left=\"" _ml "\"/>\n"                          \
	"</style:list-level-properties>\n"                            \
	"</text:list-level-style-number>\n"

#define ODT_LIST_LEVEL_STYLE_BLT(_l, _b, _i, _ml)                     \
	"<text:list-level-style-bullet text:level=\"" _l "\"\n"       \
	"    text:bullet-char=\"" _b "\">\n"                          \
	"<style:list-level-properties\n"                              \
	"    text:list-level-position-and-"                           \
	"space-mode=\"label-alignment\">\n"                           \
	"<style:list-level-label-alignment\n"                         \
	"    text:label-followed-by=\"space\"\n"                      \
	"    fo:text-indent=\"" _i "\"\n"                             \
	"    fo:margin-left=\"" _ml "\"/>\n"                          \
	"</style:list-level-properties>\n"                            \
	"</text:list-level-style-bullet>\n"

#define F_N "1"
#define F_C "а, б, .., аа, аб, .. (ru)"

#define ODT_LIST_STYLE_NUMBERING_123(D)                               \
	ODT_LIST_STYLE_BEGIN("Numbering_20_123", "Numbering 123")     \
	"{{#ListStyle}}\n"                                            \
	ODT_LIST_LEVEL_STYLE_NUM(                                     \
		"{{level}}", D,                                       \
		"{{style}}", "{{indent}}", "{{margin}}")              \
	"{{/ListStyle}}\n"                                            \
	ODT_LIST_STYLE_END

#define ODT_LIST_STYLE_LIST_1                                         \
	ODT_LIST_STYLE_BEGIN("List_20_1", "List 1")                   \
	"{{#ListStyle}}\n"                                            \
	ODT_LIST_LEVEL_STYLE_BLT(                                     \
		"{{level}}", "—",                                     \
		"{{indent}}", "{{margin}}")                           \
	"{{/ListStyle}}\n"                                            \
	ODT_LIST_STYLE_END

#define ODT_STYLES                                                     \
	"<office:styles>\n"                                            \
	ODT_DEFAULT_STYLE_PARAGRAPH                                    \
	"<style:default-style style:family=\"table-row\">\n"           \
	"<style:table-row-properties fo:keep-together=\"always\"/>\n"  \
	"</style:default-style>\n"                                     \
	ODT_STYLE_STANDARD                                             \
	ODT_STYLE_HEADING                                              \
	ODT_STYLE_TEXT_BODY                                            \
	ODT_STYLE_PREFORMATTED_TEXT                                    \
	ODT_STYLE_HEADING_n_BREAK(1, "bold", "normal")                 \
	ODT_STYLE_HEADING_n(2, "bold", "italic")                       \
	ODT_STYLE_HEADING_n(3, "normal", "normal")                     \
	ODT_STYLE_HEADING_n(4, "normal", "italic")                     \
	ODT_STYLE_HEADING_n(5, "normal", "italic")                     \
	ODT_STYLE_HEADING_n(6, "normal", "italic")                     \
	"<style:style style:name=\"Heading_20_1_20_centered\"\n"       \
	"    style:display-name=\"Heading 1 centered\"\n"              \
	"    style:family=\"paragraph\"\n"                             \
	"    style:class=\"text\"\n"                                   \
	"    style:list-style-name=\"\"\n"                             \
	"    style:parent-style-name=\"Heading_20_1\"\n"               \
	"    style:next-style-name=\"Text_20_body\">\n"                \
	"<style:paragraph-properties\n"                                \
	"    fo:text-indent=\"0mm\"\n"                                 \
	"    fo:break-after=\"page\"\n"                                \
	"    fo:text-align=\"center\"/>\n"                             \
	"<style:text-properties\n"                                     \
	"    fo:text-transform=\"uppercase\"/>\n"                      \
	"</style:style>\n"                                             \
	ODT_STYLE_TABLE_CONTENTS                                       \
	ODT_STYLE_TABLE_HEADING                                        \
	"<style:style style:name=\"Header_20_and_20_Footer\"\n"        \
	"    style:display-name=\"Header and Footer\"\n"               \
	"    style:family=\"paragraph\"\n"                             \
	"    style:parent-style-name=\"Standard\"\n"                   \
	"    style:class=\"extra\">\n"                                 \
	"<style:paragraph-properties>\n"                               \
	"<style:tab-stops>\n"                                          \
	"<style:tab-stop style:position=\"{{FooterCenterTabStop}}\" "  \
	"style:type=\"center\"/>\n"                                    \
	"<style:tab-stop style:position=\"{{FooterRightTabStop}}\" "   \
	"style:type=\"right\"/>\n"                                     \
	"</style:tab-stops>\n"                                         \
	"</style:paragraph-properties>\n"                              \
	"</style:style>\n"                                             \
	"{{#OutlineNumber}}\n"                                         \
	ODT_OUTLINE_STYLE_OUTLINE                                      \
	"{{/OutlineNumber}}\n"                                         \
	ODT_LIST_STYLE_NUMBERING_123(".")                              \
	ODT_LIST_STYLE_LIST_1                                          \
	"</office:styles>\n"

#define ODT_PAGE_LAYOUT_TEMPLATE                                       \
	"<style:page-layout style:name=\"Standard\">\n"                \
	"<style:page-layout-properties\n"                              \
	"    fo:page-width=\"{{PageWidth}}\"\n"                        \
	"    fo:page-height=\"{{PageHeight}}\"\n"                      \
	"    fo:margin-left=\"{{MarginLeft}}\"\n"                      \
	"    fo:margin-right=\"{{MarginRight}}\"\n"                    \
	"    fo:margin-top=\"{{MarginTop}}\"\n"                        \
	"    fo:margin-bottom=\"{{MarginBottom}}\"/>\n"                \
	"<style:header-style/>\n"                                      \
	"<style:footer-style>\n"                                       \
	"<style:header-footer-properties\n"                            \
	"    fo:min-height=\"1cm\"/>\n"                                \
	"</style:footer-style>\n"                                      \
	"</style:page-layout>\n"

#define ODT_MASTER_PAGE_TEMPLATE                                       \
	"<style:master-page style:name=\"Standard\"\n"                 \
	"    style:page-layout-name=\"Standard\">\n"                   \
	"{{^NoFooter}}\n"                                              \
	"<style:footer>\n"                                             \
	"<text:p text:style-name=\"Footer\">"                          \
	"<text:tab />"                                                 \
	"<text:page-number text:select-page=\"current\">2"             \
	"</text:page-number></text:p>\n"                               \
	"</style:footer>\n"                                            \
	"{{^NoFirstPageNumber}}\n"                                     \
	"<style:footer-first>\n"                                       \
	"<text:p/>\n"                                                  \
	"</style:footer-first>\n"                                      \
	"{{/NoFirstPageNumber}}\n"                                     \
	"{{/NoFooter}}\n"                                              \
	"</style:master-page>\n"

#define ODT_BLOCKQUOTE_STYLE                                           \
	"<style:style style:name=\"Typeset.BQ\"\n"                     \
	"    style:family=\"table-cell\">\n"                           \
	"<style:table-cell-properties\n"                               \
	"    fo:border-left=\"2pt solid #000000\"\n"                   \
	"    fo:padding-left=\"1cm\"/>\n"                              \
	"</style:style>\n"

#define ODT_WEAK_EMPHASIS_STYLE                                        \
	"<style:style style:name=\"T1\" style:family=\"text\">\n"      \
	"<style:text-properties fo:font-style=\"italic\"/>\n"          \
	"</style:style>\n"

#define ODT_STRONG_EMPHASIS_STYLE                                      \
	"<style:style style:name=\"T2\" style:family=\"text\">\n"      \
	"<style:text-properties fo:font-weight=\"bold\"/>\n"           \
	"</style:style>\n"

#define ODT_DOUBLE_EMPHASIS_STYLE                                      \
	"<style:style style:name=\"T3\" style:family=\"text\">\n"      \
	"<style:text-properties fo:font-weight=\"bold\"\n"             \
	"    fo:font-style=\"italic\"/>\n"                             \
	"</style:style>\n"

#define ODT_CODE_SPAN_STYLE                                            \
	"<style:style style:name=\"T4\" style:family=\"text\">\n"      \
	"<style:text-properties\n"                                     \
	"    style:font-name=\"{{ModernFont}}\"\n"                     \
	"    fo:font-size=\"{{RomanFontSize}}\"/>\n"                   \
	"</style:style>\n"

#define ODT_TYPESET_FIGPAR_STYLE                                       \
	"<style:style style:name=\"Typeset.Figpar\"\n"                 \
	"    style:family=\"paragraph\"\n"                             \
	"    style:parent-style-name=\"Text_20_body\">\n"              \
	"<style:paragraph-properties\n"                                \
	"    fo:text-align=\"center\"\n"                               \
	"    fo:line-height=\"100%\"\n"                                \
	"    fo:text-indent=\"0mm\"/>\n"                               \
	"</style:style>\n"

#define ODT_AUTO_STYLES                                                \
	"<office:automatic-styles>\n"                                  \
	ODT_BLOCKQUOTE_STYLE                                           \
	ODT_WEAK_EMPHASIS_STYLE                                        \
	ODT_STRONG_EMPHASIS_STYLE                                      \
	ODT_DOUBLE_EMPHASIS_STYLE                                      \
	ODT_CODE_SPAN_STYLE                                            \
	ODT_TYPESET_FIGPAR_STYLE                                       \
	"</office:automatic-styles>\n"

#define FLAT_ODT_AUTO_STYLES                                           \
	"<office:automatic-styles>\n"                                  \
	ODT_PAGE_LAYOUT_TEMPLATE                                       \
	ODT_BLOCKQUOTE_STYLE                                           \
	ODT_WEAK_EMPHASIS_STYLE                                        \
	ODT_STRONG_EMPHASIS_STYLE                                      \
	ODT_DOUBLE_EMPHASIS_STYLE                                      \
	ODT_CODE_SPAN_STYLE                                            \
	ODT_TYPESET_FIGPAR_STYLE                                       \
	"</office:automatic-styles>\n"

#define ODT_DOCUMENT_STYLES_TEMPLATE                                   \
	XML_SPEC                                                       \
	ODT_DOCUMENT_STYLES_BEGIN                                      \
	ODT_FONT_FACE_DECLS                                            \
	ODT_STYLES                                                     \
	"<office:automatic-styles>\n"                                  \
	ODT_PAGE_LAYOUT_TEMPLATE                                       \
	"</office:automatic-styles>\n"                                 \
	ODT_MASTER_PAGE_TEMPLATE                                       \
	ODT_DOCUMENT_STYLES_END

#define ODT_DOCUMENT_CONTENT_HEADER_TEMPLATE                           \
	XML_SPEC                                                       \
	ODT_DOCUMENT_CONTENT_BEGIN                                     \
	ODT_FONT_FACE_DECLS                                            \
	ODT_AUTO_STYLES

#define FLAT_ODT_DOCUMENT_HEADER_TEMPLATE                              \
	XML_SPEC                                                       \
	FLAT_ODT_DOCUMENT_BEGIN                                        \
	ODT_FONT_FACE_DECLS                                            \
	ODT_STYLES                                                     \
	FLAT_ODT_AUTO_STYLES                                           \
	ODT_MASTER_PAGE_TEMPLATE

extern const char odt_document_styles_template[];
extern const char odt_document_content_header_template[];
extern const char flat_odt_document_header_template[];

#endif

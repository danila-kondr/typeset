/**
 *  This file is a part of odtgen, program which converts MD to ODT.
 *  Copyright (C) 2022  Danila Kondratenko <dan.kondratenko2013@ya.ru>
 *
 *  odtgen is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  odtgen is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _ODT_SCHEMA_H_
#define _ODT_SCHEMA_H_

#define ODT_BLOCK_QUOTE_CELL_STYLE "Typeset.BQ"

#define ODT_UNORDERED_LIST_STYLE   "List_20_1"
#define ODT_ORDERED_LIST_STYLE     "Numbering_20_123"

#define ODT_CODE_BLOCK_STYLE       "Preformatted_20_Text"
#define ODT_PARAGRAPH_STYLE        "Text_20_body"
#define ODT_TABLE_HEADING_STYLE    "Table_20_Heading"
#define ODT_TABLE_CONTENTS_STYLE   "Table_20_Contents"

#define ODT_EM                     "T1"
#define ODT_STRONG                 "T2"
#define ODT_DOUBLE                 "T3"
#define ODT_CODE                   "Source_20_Text"

#define ODT_FIGURE_STYLE           "Typeset.Figpar"

#define FLAT_ODT_DOCUMENT_END      "</office:document>"
#define ODT_DOCUMENT_CONTENT_END   "</office:document-content>"

#define ODT_MIMETYPE          "application/vnd.oasis.opendocument.text"

#define ODT_BODY_BEGIN             "<office:body>\n<office:text>\n"
#define ODT_BODY_END               "</office:text>\n</office:body>\n"

#define ODT_BLOCK_QUOTE_BEGIN                                         \
	"<table:table><table:table-row><table:table-cell "            \
	"table:style-name=\"" ODT_BLOCK_QUOTE_CELL_STYLE "\">\n"

#define ODT_BLOCK_QUOTE_END                                           \
	"</table:table-cell></table:table-row></table:table>\n"

#define ODT_UNORDERED_LIST_BEGIN                                      \
	"<text:list text:style-name=\""                               \
	ODT_UNORDERED_LIST_STYLE "\">\n"

#define ODT_ORDERED_LIST_BEGIN                                        \
	"<text:list text:style-name=\""                               \
	ODT_ORDERED_LIST_STYLE "\">\n"

#define ODT_LIST_END               "</text:list>\n"

#define ODT_LIST_ITEM_BEGIN        "<text:list-item>"
#define ODT_LIST_ITEM_END          "</text:list-item>\n"

#define ODT_HEADING_BEGIN_FORMAT                                      \
	"<text:h text:outline-level=\"%d\" "                          \
	"text:style-name=\"Heading_20_%d\">"

#define ODT_HEADING_END            "</text:h>\n"

#define ODT_CODE_BLOCK_BEGIN                                          \
	"<text:p text:style-name=\"" ODT_CODE_BLOCK_STYLE "\">"

#define ODT_CODE_BLOCK_END         "</text:p>\n"

#define ODT_PARAGRAPH_BEGIN                                           \
	"<text:p text:style-name=\"" ODT_PARAGRAPH_STYLE "\">"

#define ODT_PARAGRAPH_END          "</text:p>\n"

#define ODT_TABLE_BEGIN            "<table:table>\n"
#define ODT_TABLE_END              "</table:table>\n"
#define ODT_TABLE_COLUMN           "<table:table-column/>\n"
#define ODT_TABLE_HEADER_BEGIN     "<table:table-header-rows>\n"
#define ODT_TABLE_HEADER_END       "</table:table-header-rows>\n"
#define ODT_TABLE_BODY_BEGIN       "<table:table-rows>\n"
#define ODT_TABLE_BODY_END         "</table:table-rows>\n"
#define ODT_TABLE_ROW_BEGIN        "<table:table-row>\n"
#define ODT_TABLE_ROW_END          "</table:table-row>\n"
#define ODT_TABLE_HEAD_CELL_BEGIN                                     \
	"<table:table-cell>\n"                                        \
	"<text:p text:style-name=\"" ODT_TABLE_HEADING_STYLE "\">\n"

#define ODT_TABLE_CELL_BEGIN                                          \
	"<table:table-cell>\n"                                        \
	"<text:p text:style-name=\"" ODT_TABLE_CONTENTS_STYLE "\">\n"
#define ODT_TABLE_CELL_END         "</text:p>\n</table:table-cell>\n"

#define ODT_SPAN_FIRST_HALF        "<text:span"
#define ODT_SPAN_EM_BEGIN          " text:style-name=\"" ODT_EM "\">"
#define ODT_SPAN_STRONG_BEGIN      " text:style-name=\"" ODT_STRONG "\">"
#define ODT_SPAN_DOUBLE_BEGIN      " text:style-name=\"" ODT_DOUBLE "\">"

#define ODT_LINK_FIRST_HALF        "<text:a xlink:href=\""
#define ODT_LINK_BEGIN_TAIL        "\">"

#define ODT_CODE_SPAN_BEGIN                                           \
	"<text:span text:style-name=\"" ODT_CODE "\">"

#define ODT_SPAN_END               "</text:span>"
#define ODT_LINK_END               "</text:a>"

#define ODT_LINE_BREAK             "<text:line-break/>"

#define ODT_FRAME_GRAPHICS_BEGIN_FORMAT                               \
	"<draw:frame draw:style-name=\"Graphics\""                    \
	" svg:width=\"%ldpx\" svg:height=\"%ldpx\""                   \
	" text:anchor-type=\"as-char\">\n"

#define ODT_FRAME_END              "</draw:frame>\n"

#define FLAT_ODT_IMAGE_BEGIN       "<draw:image>\n"
#define FLAT_ODT_IMAGE_END         "</draw:image>\n"

#define ODT_IMAGE_BEGIN_FORMAT     "<draw:image xlink:href=\"%s\"/>\n"

#define ODT_BINARY_DATA_BEGIN      "<office:binary-data>\n"
#define ODT_BINARY_DATA_END        "</office:binary-data>\n"

#define ODT_FRAME_FORMULA_BEGIN_FORMAT                                \
	"<draw:frame draw:style-name=\"Formula\""                     \
	" svg:width=\"%ldpx\" svg:height=\"%ldpx\""                   \
	" text:anchor-type=\"as-char\">\n"

#define ODT_TAB                    "<text:tab/>"

#define ODT_FIGURE_BEGIN                                              \
	"<text:p text:style-name=\"" ODT_FIGURE_STYLE "\">\n"

#define ODT_FIGURE_END             "</text:p>\n"

#define ODT_CAPTION_BEGIN          "<text:p text:style-name=\"Caption\">"

#endif
